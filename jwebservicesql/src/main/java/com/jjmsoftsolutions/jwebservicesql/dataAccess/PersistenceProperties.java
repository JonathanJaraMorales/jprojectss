package com.jjmsoftsolutions.jwebservicesql.dataAccess;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import com.jjmsoftsolutions.jcommon.generic.Utils;

public class PersistenceProperties {
	
	public PersistenceProperties(){
		init();
	}
	
	private Properties properties = null;

	public String getUser() {
		return properties.getProperty("javax.persistence.jdbc.user");
	}

	public String getPassword() {
		return properties.getProperty("javax.persistence.jdbc.password");
	}


	public String getDriver() {
		return properties.getProperty("javax.persistence.jdbc.driver");
	}


	public String getUrl() {
		if(Utils.isProductionEnvironment()){
			return properties.getProperty("javax.persistence.jdbc.url.prod"); 
		}
		return properties.getProperty("javax.persistence.jdbc.url");
	}

	private Properties init(){
		if(properties == null){
			properties = new Properties();
			InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties");  
			try {
				properties.load(input);
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return properties;
	}


}
