package com.jjmsoftsolutions.jcommon.exceptions;

public class JException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	final int code;

	public JException(String message) {
		super(message);
		code = -1;
	}

	public JException(int code, String msg) {
		super(msg);
		this.code = code;
	}

	public JException(String msg, Throwable t) {
		super(msg, t);
		this.code = -4;
	}

	public JException(int code, String msg, Throwable t) {
		super(msg, t);
		this.code = code;
	}

	public int getCode() {
		return code;
	}

}
