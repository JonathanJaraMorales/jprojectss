/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import com.jjmsoftsolutions.jblogger.domain.LocaleLanguages;
import com.jjmsoftsolutions.jblogger.utils.JSFUtils;

/**
 * Allows to control the Locale Language in the application
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@Named("localeController")
@ManagedBean
public class LocaleController implements Serializable {

	private static final long serialVersionUID = 1L;
	private String locale;
	private static List<LocaleLanguages> languages;
	@Inject private ParameterController parameter;

	static {
		languages = new ArrayList<LocaleLanguages>();
		languages.add(new LocaleLanguages("es", "Spain", "ES"));
		languages.add(new LocaleLanguages("de", "Germany", "GE"));
		languages.add(new LocaleLanguages("en", "United States","US"));
	}

	/**
	 * Load the all available languages by county
	 * @see java.util.List;
	 * @see com.jjmsoftsolutions.website.domain.LocaleLanguages;
	 * @return a list of languages availables
	 */
	public List<LocaleLanguages> getCountries() {
		return languages;
	}
	
	/**
	 * 
	 * @return the locale language
	 */
	public String getLocale() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		if(locale == null || session == null || session.getAttribute("locale") == null){
			session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
			session.setAttribute("locale", parameter.getParameterByCode("LANGUAGE_DEFAULT").getContent().toUpperCase());
			FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale((String) session.getAttribute("locale")));
			setLocale(FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage());
		}
		return (String) session.getAttribute("locale");
	}

	/**
	 * Set the locale language
	 * @param locale
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}
	
	/**
	 * Change the language in the application
	 * @param localeCode a value to specify the language to set .
	 */
	public void changeLocale(String localeCode) {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.setAttribute("locale", localeCode);
		FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale((String) session.getAttribute("locale")));
		setLocale(localeCode);
		JSFUtils.refresh();
	}
}
