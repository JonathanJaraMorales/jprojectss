package com.jjmsoftsolutions.jwebservicesql.entity.dao;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.PostVideoJPAImpl;

@JsonDeserialize(contentAs=List.class, as=PostVideoJPAImpl.class)
@JsonSerialize(contentAs=List.class, as=PostVideoJPAImpl.class)
public interface PostVideo extends Serializable {
	
	public int getId();
	public void setId(int id);
	
	public String getCreatedDate();
	public void setCreatedDate(String createdDate);
	
	public SuperPost getSuperPost();
	public void setSuperPost(SuperPost post);

	public String getType();
	public void setType(String type);

	public String getUrl();
	public void setUrl(String url);

}