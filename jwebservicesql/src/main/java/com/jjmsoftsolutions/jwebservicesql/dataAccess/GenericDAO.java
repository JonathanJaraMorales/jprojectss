package com.jjmsoftsolutions.jwebservicesql.dataAccess;

import java.util.Map;

public interface GenericDAO<T> extends GeneralDAO<T>{
	public int count(String sql, Map<String, Object> params);
}