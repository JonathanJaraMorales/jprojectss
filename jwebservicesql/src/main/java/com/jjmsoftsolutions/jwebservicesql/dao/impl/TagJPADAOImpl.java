package com.jjmsoftsolutions.jwebservicesql.dao.impl;

import java.util.HashMap;
import java.util.List;
import javax.inject.Named;
import com.jjmsoftsolutions.jwebservicesql.dao.TagDAO;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.JPADAO;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.JPAMetaData;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Tag;

@Named
public class TagJPADAOImpl extends JPADAO<Tag> implements TagDAO {

	//FIXME
	public List<Tag> findTagsByName(String tag, int begin, int end) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT P FROM ").append(JPAMetaData.TAG_TABLE_NAME).append(" P ");
		sql.append("WHERE P.name = :name ");
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("name", tag);
		List<Tag> list = findAll(sql.toString(), parameters, begin, end);
		return list;
	}

	//FIXME
	public Integer countByName(String name) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT COUNT(P) FROM Tag P ");
		sql.append("WHERE P.name = :name ");
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("name", name);
		return count(sql.toString(), parameters);
	}

}
