package com.jjmsoftsolutions.jwebservicesql.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.jjmsoftsolutions.jwebservicesql.dao.PostDAO;
import com.jjmsoftsolutions.jwebservicesql.dao.TagDAO;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.JPADAO;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.JPAMetaData;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Post;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Tag;

@Named
public class PostJPADAOImpl extends JPADAO<Post> implements PostDAO {
	
	@Inject private TagDAO tagDao;
	
	public List<Post> findByLanguage(String code, int begin, int end) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT P FROM ").append(JPAMetaData.POST_TABLE_NAME).append(" P ");
		sql.append("WHERE P.language.code = :code ");
		sql.append("ORDER BY P.id DESC ");
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("code", code);
		return findAll(sql.toString(), parameters, begin, end);
	}

	public Post findPostTranslationById(int id) {
		return findById(id);
	}

	public List<Post> findAllPostByTag(String tagName, int begin, int end) {
		List<Tag> tagList = tagDao.findTagsByName(tagName, begin, end);
		List<Post> post = new ArrayList<Post>();
		for(Tag tag : tagList){
			post.add(tag.getPost());
		}
		return post;
	}

	public int countByLanguage(String code){
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT COUNT(P) FROM ").append(JPAMetaData.POST_TABLE_NAME).append(" P ");
		sql.append("WHERE P.language.code = :code ");
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("code", code);
		return count(sql.toString(), parameters);
	}

	public int countByTag(String tag) {
		return tagDao.countByName(tag);
	}
	
	@Override
	public List<Post> searchEngine(String searchEngine, int begin, int end) {
		String delimitadores= "[ .,;?!¡¿\'\"\\[\\]]+";
		String[] wordArray = searchEngine.split(delimitadores);
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT P FROM ").append(JPAMetaData.POST_TABLE_NAME).append(" P ");
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		
		int i = 1;
		for(String word : wordArray){
			if(i == 1){
				sql.append("WHERE P.title LIKE :code").append(i).append(" ");
				sql.append("OR P.content LIKE :code").append(i).append(" ");
				sql.append("OR P.description LIKE :code").append(i).append(" ");
			}else{
				sql.append("OR P.title LIKE :code").append(i).append(" ");
				sql.append("OR P.content LIKE :code").append(i).append(" ");
				sql.append("OR P.description LIKE :code").append(i).append(" ");
			}
			parameters.put(("code"+i), "%"+word+"%");
			i++;			
		}
		sql.append("ORDER BY P.id DESC ");
		
		return findAll(sql.toString(), parameters, begin, end);
	}

	@Override
	public Post findLanguageSuperPost(String language, int id) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT P FROM ").append(JPAMetaData.POST_TABLE_NAME).append(" P ");
		sql.append("WHERE P.language.code = :code ");
		sql.append("AND P.superPost.id = :id ");
		sql.append("ORDER BY P.id DESC ");
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("code", language);
		parameters.put("id", id);
		return find(sql.toString(), parameters);
	}
	
	@Override
	public int countBySearchEngine(String searchEngine) {
		String delimitadores= "[ .,;?!¡¿\'\"\\[\\]]+";
		String[] wordArray = searchEngine.split(delimitadores);
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT COUNT(P) FROM ").append(JPAMetaData.POST_TABLE_NAME).append(" P ");
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		
		int i = 1;
		for(String word : wordArray){
			if(i == 1){
				sql.append("WHERE P.title LIKE :code").append(i).append(" ");
				sql.append("OR P.content LIKE :code").append(i).append(" ");
				sql.append("OR P.description LIKE :code").append(i).append(" ");
			}else{
				sql.append("OR P.title LIKE :code").append(i).append(" ");
				sql.append("OR P.content LIKE :code").append(i).append(" ");
				sql.append("OR P.description LIKE :code").append(i).append(" ");
			}
			parameters.put(("code"+i), "%"+word+"%");
			i++;			
		}
		sql.append("ORDER BY P.id DESC ");
		return count(sql.toString(), parameters);
	}
	
	/**
	 * Get the number of syllables for a given word
	 * @param s the given word
	 * @return the number of syllables
	 */
	public static int getNumberOfSyllables(String s) {
	    s = s.trim();
	    if (s.length() <= 3) {
	        return 1;
	    }
	    s = s.toLowerCase();
	    s = s.replaceAll("[aeiouy]+", "a");
	    s = "x" + s + "x";
	    return s.split("a").length - 1;
	}

}
