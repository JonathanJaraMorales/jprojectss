package com.jjmsoftsolutions.jwebservicesql.entity.parser.dao;

import com.jjmsoftsolutions.jwebservicesql.entity.dao.Post;

public interface PostParser {
	
	public Post jsonToPost(String json);

}
