package com.jjmsoftsolutions.jblogger.domain.dao;

import java.io.Serializable;
import java.util.List;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import com.jjmsoftsolutions.jblogger.domain.SuperPostImpl;

@JsonDeserialize(as = SuperPostImpl.class)
public interface SuperPost extends Serializable {
	
	public int getId();
	public void setId(int id);

	public List<Post> getPost();
	public void setPost(List<Post> post);
	
	public List<Language> getLanguages();
	public void setLanguages(List<Language> languages);
	
	public String getPicture();
	public void setPicture(String picture);
	
	public List<PostVideo> getVideos();
	public void setVideos(List<PostVideo> videos);
}
