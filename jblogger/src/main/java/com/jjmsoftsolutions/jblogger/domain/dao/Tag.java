package com.jjmsoftsolutions.jblogger.domain.dao;

import java.io.Serializable;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import com.jjmsoftsolutions.jblogger.domain.TagImpl;


@JsonDeserialize(as = TagImpl.class)
public interface Tag extends Serializable{
	
	public Post getPost();
	public void setPost(Post post);

	public int getId();
	public void setId(int id);

	public String getName();
	public void setName(String name);
}
