package com.jjmsoftsolutions.jcommon.exceptions;

import com.jjmsoftsolutions.jcommon.generic.ResponseStatus;

public class ConstraintException extends JException {

	private static final long serialVersionUID = 1L;

	public ConstraintException() {
		super("Already exist an record with the same key");
	}
	
	@Override
	public int getCode(){
		return ResponseStatus.CONFLICT.getStatus();
	}

}
