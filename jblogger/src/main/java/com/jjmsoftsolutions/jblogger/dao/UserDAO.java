package com.jjmsoftsolutions.jblogger.dao;

import com.jjmsoftsolutions.jblogger.domain.dao.User;

public interface UserDAO {
	public boolean insert(User user);
	public User findUser(String email, String password);
}
