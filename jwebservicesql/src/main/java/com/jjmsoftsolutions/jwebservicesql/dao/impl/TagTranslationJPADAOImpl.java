package com.jjmsoftsolutions.jwebservicesql.dao.impl;

import java.util.HashMap;
import java.util.List;

import javax.inject.Named;

import com.jjmsoftsolutions.jwebservicesql.dao.TagTranslationDao;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.JPADAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.Tag_Translation;

@Named
public class TagTranslationJPADAOImpl extends JPADAO<Tag_Translation> implements TagTranslationDao {

	public List<Tag_Translation> findPrincipalTagsByLanguage(String language) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT P FROM Tag_Translation P ");
		sql.append("WHERE P.language.language_Code = :language_Code ");
		sql.append("AND P.id_Tag_Translation_Father IS NULL ");
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("language_Code", language);
		return findAll(sql.toString(), parameters);
	}

	public List<Tag_Translation> findTagsByLanguageAndTitle(String language, String title) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT P FROM Tag_Translation P ");
		sql.append("WHERE P.language.language_Code = :language_Code ");
		sql.append("AND P.title LIKE :title ");
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("language_Code", language);
		parameters.put("title", ""+title+"%");
		return findAll(sql.toString(), parameters);
	}

	public Tag_Translation findTagById(int id) {
		return findById(id);
	}


}
