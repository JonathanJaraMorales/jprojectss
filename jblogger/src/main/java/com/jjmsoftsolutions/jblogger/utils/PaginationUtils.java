package com.jjmsoftsolutions.jblogger.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import com.jjmsoftsolutions.jblogger.constants.ConstantsParam;
import com.jjmsoftsolutions.jblogger.domain.Pagination;

public class PaginationUtils {
	
	public static List<Pagination> getPagination(int totalRecords, int maxRecords){
		List<Pagination> list = new ArrayList<Pagination>();
		int pagination = StringUtils.getInt(JSFUtils.getParameterURL(ConstantsParam.SIZE_PARAM));
        if (totalRecords > 0 && maxRecords > 0) {
            int totalButtons = new BigDecimal(totalRecords).divide(new BigDecimal(maxRecords), RoundingMode.UP).intValue();
            int cont = 0;
            for (int i = 0; i < totalButtons; i++) {
                cont = cont + maxRecords;
                boolean selected = i == 0 && pagination == 0 ? true : pagination == cont;
                Pagination pag = new Pagination(selected, cont);
                list.add(pag);
            }
        }
        return list;
	}

}
