package com.jjmsoftsolutions.jblogger.domain;

import javax.inject.Named;
import org.codehaus.jackson.annotate.JsonIgnore;
import com.jjmsoftsolutions.jblogger.domain.dao.Language;
import com.jjmsoftsolutions.jblogger.domain.dao.Post;
import com.jjmsoftsolutions.jblogger.domain.dao.PostAttachment;
import com.jjmsoftsolutions.jblogger.domain.dao.SuperPost;
import com.jjmsoftsolutions.jblogger.domain.dao.Tag;
import com.jjmsoftsolutions.jblogger.utils.DateUtils;
import java.util.Date;
import java.util.List;

@Named
public class PostImpl implements Post {
	private static final long serialVersionUID = 1L;

	private int id;
	private String content;
	private Date createDate;
	private String description;
	private String title;
	private Date updateDate;
	private String picture;
	private int views;
	private int likes;
	private Language language;
	private SuperPost superPost;
	private List<Tag> tags;
	private List<PostAttachment> attachments;

	public PostImpl() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Language getLanguage() {
		return this.language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
	
	public int getViews() {
		return views;
	}

	public void setViews(int views) {
		this.views = views;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public SuperPost getSuperPost() {
		return this.superPost;
	}

	public void setSuperPost(SuperPost superPost) {
		this.superPost = superPost;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
		
	}

	public List<PostAttachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<PostAttachment> attachments) {
		this.attachments = attachments;
	}
	

	@JsonIgnore
	public void reset() {
		id = 0;
		content = null;
		createDate = null;
		description = null;
		title = null;
		updateDate = null;
		language = null;
		superPost = null;
		tags = null;
		picture = null;
	}
	
	@JsonIgnore
	public String getDateMMMMMDDYYYY() {
		return DateUtils.getDateMMDDYYLetters(createDate);
	}
	
}