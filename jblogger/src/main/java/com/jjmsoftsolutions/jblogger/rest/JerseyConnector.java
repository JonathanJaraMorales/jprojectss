/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.rest;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.jackson.JacksonFeature;

import com.google.gson.Gson;
import com.jjmsoftsolutions.jblogger.annotations.Monitor;
import com.jjmsoftsolutions.jcommon.exceptions.NotFoundException;
import com.jjmsoftsolutions.jcommon.generic.Utils;

public class JerseyConnector<T> implements RestConnector<T> {

	private final String SERVLET = "api/";
	private final String CONTEXT = "jwebservicesql/";
	private final String SEPARATOR = "/";
	private Class<T> clazz;

	@SuppressWarnings("unchecked")
	public JerseyConnector() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		clazz = (Class<T>) pt.getActualTypeArguments()[0];
	}
	

	private Client getClient() {
		Client client = ClientBuilder.newClient();
		client.register(JacksonFeature.class);
		return client;
	}
	
	private String getURL(){
		return Utils.isProductionEnvironment() ? "http://www.jjmsoftsolutions.com/" :"http://localhost:8080/";
	}

	private WebTarget getWebResource(String pathService, String pathMethod, Object... object) {
		StringBuilder parameters = new StringBuilder();
		for (Object value : object) {
			parameters.append(SEPARATOR).append(value);
		}
		String url = getURL().concat(CONTEXT).concat(SERVLET).concat(pathService).concat(pathMethod).concat(parameters.toString());
		UriBuilder uriBuilder = UriBuilder.fromUri(url);
		WebTarget webTarget = getClient().target(uriBuilder);
		webTarget.register(new LoggingFilter());
		return webTarget;
	}

	@Monitor
	public List<T> findAll(String pathService, String pathMethod, Object... param) {
		WebTarget webTarget = getWebResource(pathService, pathMethod, param);
		Builder request = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = request.get();		
		List<T> list = null;
		int status = request.get().getStatus();
		if(status == 200){
			ObjectMapper mapper = new ObjectMapper();
			try {
				list = mapper.readValue(response.readEntity(String.class), mapper.getTypeFactory().constructCollectionType(List.class, clazz));	
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if(status != 200) {
			throw new NotFoundException(request.get().readEntity(String.class), status);
		}
		return list;
	}

	@Monitor
	public T find(String pathService, String pathMethod, Object... param) {
		WebTarget webTarget = getWebResource(pathService, pathMethod, param);
		Builder request = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = request.get();
		int status = response.getStatus();
		if(status != 200) {
			throw new NotFoundException(response.readEntity(String.class), status);
		}
		return response.readEntity(clazz);
	}

	public boolean insert(String pathService, String pathMethod, Object object) {
		Gson gson = new Gson();
		String json = gson.toJson(object);
		Response response = getWebResource(pathService, pathMethod).request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(json, MediaType.APPLICATION_JSON_TYPE));
		int status = response.getStatus();
		if(status != 200) {
			throw new NotFoundException(response.readEntity(String.class), status);
		}
		return true;
	}

	public int count(String pathService, String pathMethod, Object... param) {
		WebTarget webTarget = getWebResource(pathService, pathMethod, param);
		Builder request = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = request.get();
		Integer count = response.readEntity(Integer.class);
		return count;
	}

	@Override
	public boolean delete(String pathService, String pathMethod, Object object) {
		Response response = getWebResource(pathService, pathMethod, object).request(MediaType.APPLICATION_JSON_TYPE).delete();
		int status = response.getStatus();
		if(status != 200) {
			throw new NotFoundException(response.readEntity(String.class), status);
		}
		return true;
	}
	
	public void update(String pathService, String pathMethod, Object object){
		Gson gson = new Gson();
		String json = gson.toJson(object);
		Response response = getWebResource(pathService, pathMethod).request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(json, MediaType.APPLICATION_JSON_TYPE));
		System.out.println(response);
	}

}
