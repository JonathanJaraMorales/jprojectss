package com.jjmsoftsolutions.jblogger.controller;

import java.io.IOException;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.jjmsoftsolutions.jblogger.domain.dao.User;
import com.jjmsoftsolutions.jblogger.model.LoginModel;
import com.jjmsoftsolutions.jblogger.utils.JSFUtils;
import com.jjmsoftsolutions.jblogger.utils.MessageUtils;
import com.jjmsoftsolutions.jcommon.utils.Crypto;

@Controller("loginController")
@Scope("session")
public class LoginController {
	
	@Inject private LoginModel model;
	@Inject private User user;
	@Inject private MessageUtils message;
	
	private String email;
	private String password;
	private boolean isLogin;
	
	public void login() throws IOException {	
		if (email.trim().length() > 0 && password.trim().length() > 0) {
			User user = model.login(email, Crypto.md5(password));
			isLogin = false;
			if (user != null) {
				this.user = user;
				isLogin = true;
	            JSFUtils.refresh();
			}
		} else {
			error(email, password);			
		}		
	}
	
	public void signUp(){
		if (user.getEmail().trim().length() > 0 && user.getPassword().trim().length() > 0) {
			user.setNickName("nickName");
			model.insert(user);
		} else {
			error(user.getEmail(), user.getPassword());			
		}
	}
	
	public void error(String email, String password){
		if (email.trim().length() == 0) {
			message.addErrorLocalizerMessageToPage("Menu.Sign.In.Error.Email");
		}
		if (password.trim().length() == 0) {
			message.addErrorLocalizerMessageToPage("Menu.Sign.In.Error.Password");
		}
	}
	
	public void logout() throws IOException, InstantiationException, IllegalAccessException {
		isLogin = false;
		email = "";
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.invalidate();
        JSFUtils.refresh();
	}
	 
	public boolean isLogged(){
		return isLogin;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	

}
