/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.utils;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.aspectj.apache.bcel.classfile.Utility;

import com.jjmsoftsolutions.jblogger.constants.ConstantsParam;
import com.jjmsoftsolutions.jcommon.utils.Crypto;

public class JSFUtils {

	private static final int RECORDS = 3;
	
	public static String getParameterURL(String parameter) {
		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, String> paramMap = context.getExternalContext().getRequestParameterMap();
		return paramMap.get(parameter);
	}

	public static void redirect(String url) {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(url);
		} catch (IOException ex) {
			Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public static void redirectTransaction(boolean result){
		String page = result ? "success" : "error";
		redirect(page);
	}
	
	public static void refresh(){
		try {
			String currentUrl = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("referer");
			FacesContext.getCurrentInstance().getExternalContext().redirect(currentUrl);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static int getRecordsCount(){
		return StringUtils.getInt(JSFUtils.getParameterURL(ConstantsParam.SIZE_PARAM));
	}
	
	public static String getTag(){
		return JSFUtils.getParameterURL(ConstantsParam.TAG_PARAM);
	}
	
	public static int getFirstRecord(){
		int count = getRecordsCount();
		return count > 0 ? count - getMaxRecords() : 0;
	}
	
	public static int getMaxRecords(){
		return RECORDS;
	}
	
	public static String getLanguageCode(){
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		return (String) session.getAttribute("locale");
	}
	
	public static int getId(){
		String param = JSFUtils.getParameterURL(ConstantsParam.ID_PARAM);
		return StringUtils.getInt(Crypto.decode(param));
	}
}
