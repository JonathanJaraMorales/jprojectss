package com.jjmsoftsolutions.jdbupdater.update;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

public class JavaDBUpdater {

	private static final Logger LOGGER = Logger.getLogger(JavaDBUpdater.class.getName());
	
	private DataBase dataBase;
	public static int nextVersion = -1;
	public static int currentVersion = -1;
	
	public JavaDBUpdater(DataBase dataBase) {
		this.dataBase = dataBase;
		initLogging();
	}
	
	public void run(Update update){
		currentVersion++;
		if(update.execute().trim().length() > 0){
			if(nextVersion == -1){
				currentVersion++;
				nextVersion = executeDBUpdaterSelect();
				if(nextVersion == -1){
					createUpdate(createDBUpdater());
					createUpdate(insertDBUpdater());
					nextVersion = executeDBUpdaterSelect();
				}	
			}
			
			if(nextVersion == (currentVersion - 1)){
				createUpdate(update.execute());
				createUpdate(updateDBUpdater(currentVersion));
				nextVersion = executeDBUpdaterSelect();
				LOGGER.info("Running Update " + currentVersion);
			}
		}
	}
	
	public void createUpdate(String sql) {
        String[] inst = sql.split(";");  
		Connection connection = getConnection();
		try {
			Statement statement = connection.createStatement();
			for(int i = 0; i<inst.length; i++) {
				String query = inst[i].concat(";");
				if(query.trim().length() > 1){
					int result = statement.executeUpdate(query);
					LOGGER.info("Running Query " + result + " " + query);
				}
			}
			statement.close();
			connection.close();
		} catch (SQLException ex) {
			System.err.println("SQLException: " + ex.getMessage());
		}
	}
	
	public int executeDBUpdaterSelect(){
		String sql = getDBUpdater();	
		int version = -1;
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			while (rs.next()) {
				version = rs.getInt("version");
			}
			statement.close();
			connection.close();
		} catch (Exception e) {
			return version;
		} 
		return version;
	}
	
	public Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName(dataBase.getDriver());
			connection = DriverManager.getConnection(dataBase.getUrl(), dataBase.getUser(), dataBase.getPassword());
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return connection;
	}
	
	private String getDBUpdater(){
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT version FROM DBUpdater ");
		return sql.toString();
	}
	
	public String createDBUpdater(){
		StringBuilder sql = new StringBuilder();
		sql.append("CREATE TABLE IF NOT EXISTS DBUpdater ( ");
		sql.append("version int NOT NULL, ");
		sql.append("PRIMARY KEY (version) ");
		sql.append(") ENGINE=MyISAM DEFAULT CHARSET=utf8; ");
		return sql.toString();
	}
	
	public String insertDBUpdater(){
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO DBUpdater (version) VALUES (0) ");
		return sql.toString();
	}
	
	public String updateDBUpdater(int version){
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE DBUpdater SET version = ").append(version).append(" ");
		return sql.toString();
	}

	public void initLogging(){
		LOGGER.info("Start DBUpdaterServlet");
		LOGGER.info("User: " + dataBase.getUser());
		LOGGER.info("Password: " + dataBase.getPassword());
		LOGGER.info("Driver: " + dataBase.getDriver());
		LOGGER.info("Url: " + dataBase.getUrl());
	}

}
