package com.jjmsoftsolutions.jwebservicesql.dataAccess;

import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.LanguageJPAImpl;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.ParameterJPAImpl;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.PostJPAImpl;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.TagJPAImpl;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.UserJPAImpl;


public class JPAMetaData {	
	
	public static final String LANGUAGE_TABLE_NAME = "LanguageJPAImpl";
	public static final String POST_TABLE_NAME = "PostJPAImpl";
	public static final String TAG_TABLE_NAME = "TagJPAImpl";
	public static final String PARAMETER_TABLE_NAME = "ParameterJPAImpl";
	public static final String USER_TABLE_NAME = "UserJPAImpl";
	
	public static final Class<LanguageJPAImpl> LANGUAGE_TABLE = LanguageJPAImpl.class;
	public static final Class<PostJPAImpl> POST_TABLE = PostJPAImpl.class;
	public static final Class<TagJPAImpl> TAG_TABLE = TagJPAImpl.class;
	public static final Class<ParameterJPAImpl> PARAMETER_TABLE = ParameterJPAImpl.class;
	public static final Class<UserJPAImpl> USER_TABLE = UserJPAImpl.class;
	
}
