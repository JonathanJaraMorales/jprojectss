package com.jjmsoftsolutions.jblogger.controller;

import javax.inject.Named;
import com.jjmsoftsolutions.jcommon.utils.Crypto;

@Named("cryptoController")
public class CryptoController {

	public String encrypt(String data){
		try {
			return Crypto.encode(data);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
}
