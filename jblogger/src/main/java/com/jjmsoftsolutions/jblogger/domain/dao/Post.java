package com.jjmsoftsolutions.jblogger.domain.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import com.jjmsoftsolutions.jblogger.domain.PostImpl;

@JsonDeserialize(as=PostImpl.class)
public interface Post extends Serializable{

	public int getId();
	public void setId(int id);
	
	public String getContent();
	public void setContent(String content);
	
	public Date getCreateDate();
	public void setCreateDate(Date createDate);

	public String getDescription();
	public void setDescription(String description);

	public String getTitle();
	public void setTitle(String title);
	
	public Date getUpdateDate();
	public void setUpdateDate(Date updateDate);
	
	public int getViews();
	public void setViews(int views);

	public int getLikes();
	public void setLikes(int likes);

	public Language getLanguage();
	public void setLanguage(Language language);

	public SuperPost getSuperPost();
	public void setSuperPost(SuperPost post);
	
	public String getPicture();
	public void setPicture(String picture);

	public List<Tag> getTags();
	public void setTags(List<Tag> tags);
	
	public List<PostAttachment> getAttachments();
	public void setAttachments(List<PostAttachment> attachments);
	
	public String getDateMMMMMDDYYYY();
	public void reset();

}
