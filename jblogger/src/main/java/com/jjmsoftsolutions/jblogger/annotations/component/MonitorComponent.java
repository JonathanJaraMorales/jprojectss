/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.annotations.component;

import java.util.concurrent.TimeUnit;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.jjmsoftsolutions.jblogger.annotations.Monitor;
import com.jjmsoftsolutions.jblogger.annotations.type.MonitorMemoryType;
import com.jjmsoftsolutions.jblogger.annotations.type.MonitorTimeType;

/**
 * Allow to process the {@code @Monitor}
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@Aspect
@Component
public class MonitorComponent {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * Load the annotation
	 */
	@Pointcut("execution(@com.jjmsoftsolutions.website.annotations.Monitor * *.*(..))")
    public void monitor() {
    }
	
	/**
	 * Allow to process the monitor statistics
	 * @param pjp an object to expose the proceed method
	 * @param monitor an object with the configuration to show the monitor properties
	 * @return an object to has the actual process
	 * @see com.jjmsoftsolutions.jblogger.annotations.Monitor
	 * @see org.aspectj.runtime.internal.AroundClosure.ProceedingJoinPoint
	 */
	@Around("execution(* *(..)) && @annotation(monitor)")
    public Object profile(ProceedingJoinPoint pjp, Monitor monitor) {	
    	long start = System.nanoTime();
    	printMemory(monitor.memory());
        Object output = null;
		try {
			output = pjp.proceed();
		} catch (Throwable e) {
			logger.error(e.getMessage(), e);
		}
		long stop = System.nanoTime();
		System.err.println(pjp.getSignature());
		printTime(start, stop, monitor.time());
        return output;
    }

	/**
	 * Allow to process the memory
	 * @param monitor an object with the configuration to show the monitor properties
	 * @see com.jjmsoftsolutions.jblogger.annotations.Monitor
	 */
	@After("execution(* *(..)) && @annotation(monitor)")
    public void profileMemory(Monitor monitor) {
		getMemory(monitor.memory());
    }
	
	/**
	 * Create a message to show the monitor time.
	 * 
	 * @param start a time to specify the begging
	 * @param stop a time to specify the end
	 * @param time an object to validate the current selection
	 * @return message to display in the console and log
	 * @see com.jjmsoftsolutions.jblogger.annotations.type.MonitorTimeType
	 */
	private String getTime(long start, long stop, MonitorTimeType time){
		String elapsedTime =  "";
		if(time.equals(MonitorTimeType.NANO_SECONDS)){
			elapsedTime = "[Time Elapsed=" + TimeUnit.NANOSECONDS.convert((stop-start), TimeUnit.NANOSECONDS) + "]";
		}else if(time.equals(MonitorTimeType.SECONDS)){
			elapsedTime = "[Time Elapsed=" + TimeUnit.SECONDS.convert((stop-start), TimeUnit.NANOSECONDS) + "]";
		}else if(time.equals(MonitorTimeType.MINUTES)){
			elapsedTime = "[Time Elapsed=" + TimeUnit.MINUTES.convert((stop-start), TimeUnit.NANOSECONDS) + "]";
		}
		return elapsedTime;
	}
	
	/**
	 * Create a message to show the monitor memory
	 * 
	 * @param memory an object to validate the current selection
	 * @return message to display in the console and log
	 * @see com.jjmsoftsolutions.jblogger.annotations.type.MonitorMemoryType
	 */
	private String getMemory(MonitorMemoryType memory){
		if(memory.equals(MonitorMemoryType.FREE_MEMORY)){
			return "[Free Memory=" + Runtime.getRuntime().freeMemory() + "]";
		}else if(memory.equals(MonitorMemoryType.USED_MEMORY)){
			return "[Used Memory=" + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) + "]";
		}
		return "";
	}
	
	/**
	 * Print the memory message in the log and console
	 * 
	 * @param memory an object to validate the current selection
	 * @see com.jjmsoftsolutions.jblogger.annotations.type.MonitorMemoryType
	 */
	private void printMemory(MonitorMemoryType memory){
		String message = getMemory(memory);
		logger.info(message);
	    System.err.println(message);
	}
	
	/**
	 * Print the memory message in the log and console
	 * @param start a time to specify the begging
	 * @param stop a time to specify the end
	 * @param time an object to validate the current selection
	 * @see com.jjmsoftsolutions.jblogger.annotations.type.MonitorTimeType
	 */
	private void printTime(long start, long stop, MonitorTimeType time){
		String message = getTime(start, stop, time);
		logger.info(message);
	    System.err.println(message);
	}

}