package com.jjmsoftsolutions.jwebservicesql.dao;

import java.util.List;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.GeneralDAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.User;

public interface UserDAO extends GeneralDAO<User>{	
	public List<User> findAllUsers();
	public User findByEmailPassword(String email, String password);
}
