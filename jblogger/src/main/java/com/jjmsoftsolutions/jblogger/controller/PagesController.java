package com.jjmsoftsolutions.jblogger.controller;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.inject.Named;
import javax.inject.Singleton;

import com.jjmsoftsolutions.jblogger.constants.ConstantsOutcome;

@Named("pages")
@Singleton
public class PagesController implements Serializable, Map<String, String> {

	private static final long serialVersionUID = 1L;
	private Map<String, String> constantsMap;

	public Map<String, String> getConstantsMap() {
		if (constantsMap == null) {
			Map<String, String> newConstantsMap = new HashMap<String, String>();

			for (Field field : ConstantsOutcome.class.getFields()) {
				if (!Modifier.isStatic(field.getModifiers())) {
					continue;
				}
				try {
					Object result = field.get(null);
					if (result != null) {
						String resultStr = result.toString();
						if (result != null) {
							newConstantsMap.put(field.getName(), resultStr);
						}
					}
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			constantsMap = newConstantsMap;
		}
		return constantsMap;
	}

	public String get(Object key) {
		return getConstantsMap().get(key);
	}

	public int size() {
		return getConstantsMap().size();
	}

	public boolean isEmpty() {
		return getConstantsMap().isEmpty();
	}

	public boolean containsKey(Object key) {
		return getConstantsMap().containsKey(key);
	}

	public boolean containsValue(Object value) {
		return getConstantsMap().containsValue(value);
	}

	public String put(String key, String value) {
		throw new UnsupportedOperationException();
	}

	public String remove(Object key) {
		throw new UnsupportedOperationException();
	}

	public void putAll(Map<? extends String, ? extends String> m) {
		throw new UnsupportedOperationException();
	}

	public void clear() {
		throw new UnsupportedOperationException();
	}

	public Set<String> keySet() {
		return getConstantsMap().keySet();
	}

	public Collection<String> values() {
		return getConstantsMap().values();
	}

	public Set<java.util.Map.Entry<String, String>> entrySet() {
		return getConstantsMap().entrySet();
	}
}
