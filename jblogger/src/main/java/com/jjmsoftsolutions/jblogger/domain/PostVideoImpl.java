package com.jjmsoftsolutions.jblogger.domain;

import com.jjmsoftsolutions.jblogger.domain.dao.PostVideo;
import com.jjmsoftsolutions.jblogger.domain.dao.SuperPost;

public class PostVideoImpl implements PostVideo {

	private static final long serialVersionUID = 1L;
	private int id;
	private String createdDate;
	private String type;
	private String url;
	private SuperPost superPost;

	public PostVideoImpl() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public SuperPost getSuperPost() {
		return this.superPost;
	}

	public void setSuperPost(SuperPost superPost) {
		this.superPost = superPost;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
