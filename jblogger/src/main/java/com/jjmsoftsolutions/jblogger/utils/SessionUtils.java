/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.jjmsoftsolutions.jblogger.utils;

import java.util.Locale;

import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

@Named("sessionUtils")
public class SessionUtils {

	public String getLocale() {
		/*
		 * HttpSession session = (HttpSession)
		 * FacesContext.getCurrentInstance().
		 * getExternalContext().getSession(false); return (String)
		 * session.getAttribute("locale");
		 */
		FacesContext facesContext = FacesContext.getCurrentInstance();
		Locale locale = facesContext.getViewRoot().getLocale();
		return locale.getLanguage();
	}

	public HttpSession getSession() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(false);
		return session;
	}
	
	public static String getLanguageName(String languageCode){
		languageCode = languageCode.toLowerCase();
		String language = FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
		if(language.equals("es")){
			if(languageCode.equals("es")){
				return "Español";
			}else if(languageCode.equals("de")){
				return "Aleman";
			}else if(languageCode.equals("en")){
				return "Inglés";
			}
		}else if(language.equals("de")){
			if(languageCode.equals("es")){
				return "Spanish";
			}else if(languageCode.equals("de")){
				return "German";
			}else if(languageCode.equals("en")){
				return "English";
			}
		}else if(language.equals("en")){
			if(languageCode.equals("es")){
				return "Spanish";
			}else if(languageCode.equals("de")){
				return "German";
			}else if(languageCode.equals("en")){
				return "English";
			}
		}
		return language;
	}

}
