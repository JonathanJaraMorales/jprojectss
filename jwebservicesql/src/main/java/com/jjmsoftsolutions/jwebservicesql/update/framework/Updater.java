package com.jjmsoftsolutions.jwebservicesql.update.framework;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public abstract class Updater {
	
	public String getScript(String fileName) {
		String path = Thread.currentThread().getContextClassLoader().getResource("").getPath()+ "com/jjmsoftsolutions/jwebservicesql/update/sql/" + fileName;
		File file = new File(path);
		String script = "";
		try {
			script = new Scanner(file).useDelimiter("\\A").next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return script;
	}
	
}
