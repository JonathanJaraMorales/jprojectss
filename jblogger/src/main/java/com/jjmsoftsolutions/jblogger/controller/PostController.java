/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.controller;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import com.jjmsoftsolutions.jblogger.constants.ConstantsOutcome;
import com.jjmsoftsolutions.jblogger.constants.ConstantsParam;
import com.jjmsoftsolutions.jblogger.domain.Pagination;
import com.jjmsoftsolutions.jblogger.domain.TagImpl;
import com.jjmsoftsolutions.jblogger.domain.dao.Language;
import com.jjmsoftsolutions.jblogger.domain.dao.Post;
import com.jjmsoftsolutions.jblogger.domain.dao.PostAttachment;
import com.jjmsoftsolutions.jblogger.domain.dao.SuperPost;
import com.jjmsoftsolutions.jblogger.domain.dao.Tag;
import com.jjmsoftsolutions.jblogger.model.LanguageModel;
import com.jjmsoftsolutions.jblogger.model.PostModel;
import com.jjmsoftsolutions.jblogger.utils.FTPUtils;
import com.jjmsoftsolutions.jblogger.utils.JSFUtils;
import com.jjmsoftsolutions.jblogger.utils.PaginationUtils;
import com.jjmsoftsolutions.jblogger.utils.StringUtils;
import com.jjmsoftsolutions.jcommon.utils.Crypto;

@Controller("postController")
@Scope("request")
public class PostController implements Serializable {

	private static final long serialVersionUID = 1L;
	@Inject private Post post;	
	@Inject private SuperPost superPost;	
	@Inject private PostModel model;
	@Inject private LanguageModel languageModel;
	@Inject private AttachmentController attachmentController;
	@Inject private ParameterController parameterController;
	@Inject private FTPUtils ftp;
	
	private String tags;
	private String searchEngine;
	private boolean alreadyExist;
	private String languageCode;
	private int mode;
	private final int EDIT_MODE = 1;
	
	private List<Post> searchEnginePost;

	public List<Post> getPopulatePostByLanguage() {
		return model.findAllByLanguage(JSFUtils.getLanguageCode(), JSFUtils.getFirstRecord(), JSFUtils.getMaxRecords());
	}
	
	public List<Post> getPopulatePostByTagName(){
		return model.findAllPostByTag(JSFUtils.getTag(), JSFUtils.getFirstRecord(), JSFUtils.getMaxRecords());
	}
	
	public List<Post> getPopulatePostByTagId(){
		return model.findAllPostByTagId(StringUtils.getInt(Crypto.decode(JSFUtils.getTag())), JSFUtils.getFirstRecord(), JSFUtils.getMaxRecords());
	}
	
	public void loadSearchEngine(){
		JSFUtils.redirect(ConstantsOutcome.POST_SEARCH_ENGINE.concat("?").concat(ConstantsParam.SEARCH_PARAM).concat("=").concat(searchEngine));
	}
	
	public List<Post> getPopulateSearchEngine(){
		searchEnginePost =  model.searchEngine(JSFUtils.getParameterURL(ConstantsParam.SEARCH_PARAM), JSFUtils.getFirstRecord(), JSFUtils.getMaxRecords());
		searchEngine = "";
		return searchEnginePost;
	}
	
	public Post getPostByLanguage(){
		setPost(model.getPostByLanguage(JSFUtils.getId(), JSFUtils.getParameterURL("language")));
		return getPost();
	}
	
	public List<Pagination> getPagination() {
		return PaginationUtils.getPagination(model.countByLanguage(JSFUtils.getLanguageCode()), JSFUtils.getMaxRecords());		
	}
	
	public List<Pagination> getPaginationTag() {
		return PaginationUtils.getPagination(model.countByTag(JSFUtils.getTag()), JSFUtils.getMaxRecords());
	}
	
	public List<Pagination> getPaginationSearchEngine() {
		return PaginationUtils.getPagination(model.countBySearchEngine(JSFUtils.getParameterURL(ConstantsParam.SEARCH_PARAM)), JSFUtils.getMaxRecords());
	}

	public Post getPostById() {
		if(isEditMode()){
			setPost(model.findById(JSFUtils.getId()));
			buildJqueryTags();
		}else{
			String param = JSFUtils.getParameterURL(ConstantsParam.ID_PARAM);
			languageCode = JSFUtils.getParameterURL("language") == null ? languageCode : JSFUtils.getParameterURL("language");
			alreadyExist = false;
			if(param != null){
				int id = StringUtils.getInt(Crypto.decode(param));
				if(id == 0){
					JSFUtils.redirect("404.xhtml");
				}else{
					post = model.findById(id);
					buildJqueryTags();
					alreadyExistPostWithSameLanguage();
				}
			
			}
		}
		return getPost();
	}
	
	private boolean isEditMode(){
		return mode == EDIT_MODE;
	}
	
	private void buildJqueryTags(){
		tags = "";
		for(Tag tag : post.getTags()){
			tags += tag.getName().concat(",");
		}
	}
	
	private void alreadyExistPostWithSameLanguage(){
		if(languageCode != null){
			List<Language> languagesAvailables = post.getSuperPost().getLanguages();
			int i = 0;
			int size = languagesAvailables.size();
			while(i < size && !alreadyExist){
				Language language = languagesAvailables.get(i);
				if(languageCode.equals(language.getCode())){
					alreadyExist = true;
				}
				i++;
			}
		}	
	}

	public void insertPost() throws IOException {
		Language language = languageModel.findByCode(languageCode.toLowerCase());
		post.setTags(getConverterTags());
		post.setLanguage(language);
		
		if(post.getSuperPost() == null){
			superPost.setPicture(post.getPicture());
			post.setSuperPost(superPost);
		}
		
		List<PostAttachment> attachments = attachmentController.getAttachments();
		
		
		String ftpUrl = parameterController.getParameterByCode("FTP_PUBLIC_URL").getContent().trim()
				.concat(parameterController.getParameterByCode("FTP_ATTACHMENTS_FOLDER").getContent()).trim();
		
		String ftpFolder = parameterController.getParameterByCode("FTP_ATTACHMENTS_FOLDER").getContent();
		
		ftp.setUser(parameterController.getParameterByCode("FTP_USER").getContent());
		ftp.setPassword(parameterController.getParameterByCode("FTP_PASSWORD").getContent());
		ftp.setHost(parameterController.getParameterByCode("FTP_HOST").getContent());
		
		for(PostAttachment attachment : attachments){
			 attachment.setUrl(ftpUrl);
			 ftp.upload(ftpFolder, attachment.getPart().getInputStream(), attachment.getName());
		}
		
		post.setAttachments(attachments);
		model.insert(post);
		
	}
	
	public void delete(String cryptoId){
		model.delete(StringUtils.getInt(Crypto.decode(cryptoId)));
	}
	
	private List<Tag> getConverterTags(){
		List<Tag> tags = new ArrayList<Tag>();
		for (String item : Arrays.asList(this.tags.split("\\s*,\\s*"))) {
			TagImpl tag = new TagImpl();
			tag.setName(item);
			tags.add(tag);
		}
		return tags;
	}

	public void editPost() {

	}
	
	public String getSearchEngine(){
		return searchEngine;
	}

	public void setSearchEngine(String searchEngine){
		this.searchEngine = searchEngine;
	}
	
	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public boolean isAlreadyExist() {
		return alreadyExist;
	}

	public void setAlreadyExist(boolean alreadyExist) {
		this.alreadyExist = alreadyExist;
	}

	public AttachmentController getAttachmentController() {
		return attachmentController;
	}

	public void setAttachmentController(AttachmentController attachmentController) {
		this.attachmentController = attachmentController;
	}

	public int isMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}
	
	public List<Post> getSearchEnginePost() {
		return searchEnginePost;
	}

	public void setSearchEnginePost(List<Post> searchEnginePost) {
		this.searchEnginePost = searchEnginePost;
	}
	
	public String getSearchEngineParam() throws UnsupportedEncodingException{
		String param = JSFUtils.getParameterURL(ConstantsParam.SEARCH_PARAM);
		if(param != null){
			byte ptext[] = JSFUtils.getParameterURL(ConstantsParam.SEARCH_PARAM).getBytes("ISO-8859-1"); 
			return new String(ptext, "UTF-8");
		}
		return "";
	}
}
