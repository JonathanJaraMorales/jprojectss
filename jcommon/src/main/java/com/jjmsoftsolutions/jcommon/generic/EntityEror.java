package com.jjmsoftsolutions.jcommon.generic;

public class EntityEror {
	
	public static final String POST_CONTENT_NOT_NULL = "The content is required";
	public static final String POST_DESCRIPTION_NOT_NULL = "The description is required";
	public static final String POST_TITLE_NOT_NULL = "The title is required";
	public static final String POST_PICTURE_NOT_NULL = "The picture is required";

}
