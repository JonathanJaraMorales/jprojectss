/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.dao;

import java.util.List;

import com.jjmsoftsolutions.jblogger.domain.Parameter;

/**
 * Allow to create the specification of Parameter
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 * @version 1.0
 * @since 1.0
 * @see com.jjmsoftsolutions.jblogger.domain.Parameter
 */
public interface ParameterDAO {
	/**
	 * Search a parameter by code
	 * @param code to search
	 * @return a parameter
	 * @see com.jjmsoftsolutions.jblogger.domain.Parameter
	 */
	public Parameter findByCode(String code);
	
	public List<Parameter> findAll();

	public void update(Parameter parameter);
}
