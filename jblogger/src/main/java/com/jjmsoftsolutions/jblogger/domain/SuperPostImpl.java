package com.jjmsoftsolutions.jblogger.domain;

import com.jjmsoftsolutions.jblogger.domain.dao.Language;
import com.jjmsoftsolutions.jblogger.domain.dao.Post;
import com.jjmsoftsolutions.jblogger.domain.dao.PostVideo;
import com.jjmsoftsolutions.jblogger.domain.dao.SuperPost;

import java.util.List;

import javax.inject.Named;

@Named
public class SuperPostImpl implements SuperPost {
	private static final long serialVersionUID = 1L;

	private int id;
	private List<Post> post;
	private List<Language> languages;
	private List<PostVideo> videos;

	private String picture;
	
	public SuperPostImpl() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Post> getPost() {
		return this.post;
	}

	public void setPost(List<Post> post) {
		this.post = post;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

	@Override
	public String getPicture() {
		return picture;
	}

	@Override
	public void setPicture(String picture) {
		this.picture = picture;
	}

	@Override
	public List<PostVideo> getVideos() {
		return videos;
	}

	@Override
	public void setVideos(List<PostVideo> videos) {
		this.videos = videos;
	}

}