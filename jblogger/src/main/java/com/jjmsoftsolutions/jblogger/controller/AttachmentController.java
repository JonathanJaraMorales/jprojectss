package com.jjmsoftsolutions.jblogger.controller;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import com.jjmsoftsolutions.jblogger.domain.PostAttachmentImpl;
import com.jjmsoftsolutions.jblogger.domain.dao.PostAttachment;

@Named
public class AttachmentController {
	
	private PostAttachment attachment1 = new PostAttachmentImpl();
	private PostAttachment attachment2 = new PostAttachmentImpl();
	private PostAttachment attachment3 = new PostAttachmentImpl();
	private PostAttachment attachment4 = new PostAttachmentImpl();
	private PostAttachment attachment5 = new PostAttachmentImpl();
	private PostAttachment attachment6 = new PostAttachmentImpl();
	private PostAttachment attachment7 = new PostAttachmentImpl();
	private PostAttachment attachment8 = new PostAttachmentImpl();
	private PostAttachment attachment9 = new PostAttachmentImpl();
	
	private List<PostAttachment> attachments = new ArrayList<PostAttachment>();
	
	public PostAttachment getAttachment1() {
		return attachment1;
	}

	public void setAttachment1(PostAttachment attachment1) {
		this.attachment1 = attachment1;
	}

	public PostAttachment getAttachment2() {
		return attachment2;
	}

	public void setAttachment2(PostAttachment attachment2) {
		this.attachment2 = attachment2;
	}

	public PostAttachment getAttachment3() {
		return attachment3;
	}

	public void setAttachment3(PostAttachment attachment3) {
		this.attachment3 = attachment3;
	}

	public PostAttachment getAttachment4() {
		return attachment4;
	}

	public void setAttachment4(PostAttachment attachment4) {
		this.attachment4 = attachment4;
	}

	public PostAttachment getAttachment5() {
		return attachment5;
	}

	public void setAttachment5(PostAttachment attachment5) {
		this.attachment5 = attachment5;
	}

	public PostAttachment getAttachment6() {
		return attachment6;
	}

	public void setAttachment6(PostAttachment attachment6) {
		this.attachment6 = attachment6;
	}

	public PostAttachment getAttachment7() {
		return attachment7;
	}

	public void setAttachment7(PostAttachment attachment7) {
		this.attachment7 = attachment7;
	}

	public PostAttachment getAttachment8() {
		return attachment8;
	}

	public void setAttachment8(PostAttachment attachment8) {
		this.attachment8 = attachment8;
	}

	public PostAttachment getAttachment9() {
		return attachment9;
	}

	public void setAttachment9(PostAttachment attachment9) {
		this.attachment9 = attachment9;
	}

	public List<PostAttachment> getAttachments() {
		addAttachment(attachment1);
		addAttachment(attachment2);
		addAttachment(attachment3);
		addAttachment(attachment4);
		addAttachment(attachment5);
		addAttachment(attachment6);
		addAttachment(attachment7);
		addAttachment(attachment8);
		addAttachment(attachment9);
		return attachments;
	}

	public void setAttachments(List<PostAttachment> attachments) {
		this.attachments = attachments;
	}

	public static String getFileName(PostAttachment postAttachment) {
		String header = postAttachment.getPart().getHeader("content-disposition");
		if (header == null)
			return null;
		for (String headerPostAttachment : header.split(";")) {
			if (headerPostAttachment.trim().startsWith("filename")) {
				return headerPostAttachment.substring(headerPostAttachment.indexOf('=') + 1).trim().replace("\"", "");
			}
		}
		return null;
	}

	private void addAttachment(PostAttachment postAttachment) {
		if(postAttachment.getPart() != null){
			String fileName = getFileName(postAttachment);
			String type = postAttachment.getPart().getContentType();
			Long size = postAttachment.getPart().getSize();
			postAttachment.setDescription("description");
			postAttachment.setName(fileName);
			postAttachment.setSize(size.intValue());
			postAttachment.setType(type);
			attachments.add(postAttachment);
		}	
	}
	
	public void init(){
		setAttachment1(new PostAttachmentImpl());
		setAttachment2(new PostAttachmentImpl());
		setAttachment3(new PostAttachmentImpl());
		setAttachment4(new PostAttachmentImpl());
		setAttachment5(new PostAttachmentImpl());
		setAttachment6(new PostAttachmentImpl());
		setAttachment7(new PostAttachmentImpl());
		setAttachment9(new PostAttachmentImpl());
		setAttachments(new ArrayList<PostAttachment>());
	}
	
}
