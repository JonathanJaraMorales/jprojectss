package com.jjmsoftsolutions.jblogger.utils;

import javax.faces.application.FacesMessage.Severity;

public interface MessageUtils {
	public void addMessageToPage(String summary, String details, Severity severity);
	
	public void addErrorMessageToPage(String details);
	public void addErrorLocalizerMessageToPage(String details);
	public void addErrorLocalizerMessageToPage(String details, Object... params);
	
	public void addSuccessMessageToPage(String details);
	public void addSuccessLocalizerMessageToPage(String details);
	public void addSuccessLocalizerMessageToPage(String details, Object... params);
}
