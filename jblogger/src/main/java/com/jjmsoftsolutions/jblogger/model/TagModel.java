/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.jjmsoftsolutions.jblogger.model;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.jjmsoftsolutions.jblogger.dao.TagDAO;
import com.jjmsoftsolutions.jblogger.domain.Tag_Translation;

@Named
public class TagModel {

	@Inject private TagDAO dao;
	
	public List<Tag_Translation> getTagMenuByLanguage(String language) {
		return dao.findTagMenuByLanguage(language);
	}
	
	public List<Tag_Translation> findTagsByLanguageAndTitle(String language, String title) {
		return dao.findTagsByLanguageAndTitle(language, title);
	}

}
