package com.jjmsoftsolutions.jwebservicesql.entity.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonManagedReference;

/**
 * The persistent class for the Tag_Translation database table.
 * 
 */
@Entity
@NamedQuery(name = "Tag_Translation.findAll", query = "SELECT t FROM Tag_Translation t")
@Table(name = "Tag_Translation")
public class Tag_Translation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id_Tag_Translation;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "language_Code")
	@JsonManagedReference
	private LanguageJPAImpl language;

	private String title;

	@OneToMany(fetch=FetchType.EAGER, mappedBy="id_Tag_Translation_Father", cascade=CascadeType.PERSIST)
	@JsonManagedReference
	private List<Tag_Translation> tags;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.PERSIST)
	@JoinColumn(name="id_Tag_Translation_Father", referencedColumnName="id_Tag_Translation")
	@JsonBackReference
	private Tag_Translation id_Tag_Translation_Father;
	

	public Tag_Translation getId_Tag_Translation_Father() {
		return id_Tag_Translation_Father;
	}

	public void setId_Tag_Translation_Father(
			Tag_Translation id_Tag_Translation_Father) {
		this.id_Tag_Translation_Father = id_Tag_Translation_Father;
	}

	public List<Tag_Translation> getTags() {
		return tags;
	}

	public void setTags(List<Tag_Translation> tags) {
		this.tags = tags;
	}

	public Tag_Translation() {
	}

	public int getId_Tag_Translation() {
		return this.id_Tag_Translation;
	}

	public void setId_Tag_Translation(int id_Tag_Translation) {
		this.id_Tag_Translation = id_Tag_Translation;
	}

	public LanguageJPAImpl getLanguage() {
		return this.language;
	}

	public void setLanguage(LanguageJPAImpl language) {
		this.language = language;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}



}