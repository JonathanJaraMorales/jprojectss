package com.jjmsoftsolutions.jcommon.exceptions;

public class HostException extends JException{

	private static final long serialVersionUID = 1L;

	public HostException() {
		super("Error connection to the service. Please contact to the host server");
	}
	
	@Override
	public int getCode(){
		return 400;
	}

}
