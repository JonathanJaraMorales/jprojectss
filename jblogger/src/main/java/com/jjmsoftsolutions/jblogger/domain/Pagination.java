package com.jjmsoftsolutions.jblogger.domain;

public class Pagination {
	
	public boolean isCurrent;
	public int begin;
	
	public Pagination(boolean isCurrent, int begin){
		setCurrent(isCurrent);
		setBegin(begin);
	}
	
	public boolean isCurrent() {
		return isCurrent;
	}
	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}
	public int getBegin() {
		return begin;
	}
	public void setBegin(int begin) {
		this.begin = begin;
	}

}
