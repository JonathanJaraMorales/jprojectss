package com.jjmsoftsolutions.jwebservicesql.dao.impl;

import java.util.HashMap;
import java.util.List;
import javax.inject.Named;
import com.jjmsoftsolutions.jwebservicesql.dao.UserDAO;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.JPADAO;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.JPAMetaData;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.User;

@Named
public class UserJPADAOImpl extends JPADAO<User> implements UserDAO {

	public List<User> findAllUsers() {
		return findAll();
	}

	public User findByEmailPassword(String email, String password) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT P FROM ").append(JPAMetaData.USER_TABLE_NAME).append(" P ");
		sql.append("WHERE P.email = :email ");
		sql.append("AND P.password =:password ");
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("email", email);
		parameters.put("password", password);
		return find(sql.toString(), parameters);
	}
	
}
