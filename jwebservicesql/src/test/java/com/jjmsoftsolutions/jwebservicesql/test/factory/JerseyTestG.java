package com.jjmsoftsolutions.jwebservicesql.test.factory;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;
import javax.ws.rs.core.Application;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContextTest.xml" })
@TransactionConfiguration(defaultRollback = false)
public abstract class JerseyTestG extends JerseyTest {

	private static final AtomicInteger count = new AtomicInteger(0);

	@Override
	protected void configureClient(ClientConfig clientConfig) {
		clientConfig.register(new JacksonJsonProvider());
	}

	@Override
	protected Application configure() {
		return new ResourceConfig();
	}

	public <T> T newInstance(Class<T> clazz) {
		T clazzImplementation = getTypeImplementation(clazz);
		Field[] fields = clazzImplementation.getClass().getDeclaredFields();
		for (Field field : fields) {
			if (!Modifier.isStatic(field.getModifiers())) {
				String fieldName = field.getName();
				Class<?> fieldType = field.getType();
				if (fieldType.isAssignableFrom(String.class)) {
					set(clazzImplementation, fieldName, "Inject " + fieldName+ " value");
				} else if (fieldType.isAssignableFrom(int.class)) {
					set(clazzImplementation, fieldName, count.incrementAndGet());
				} else if (fieldType.isAssignableFrom(Date.class)) {
					set(clazzImplementation, fieldName, newDate());
				} else if (fieldType.isAssignableFrom(long.class)) {
					set(clazzImplementation, fieldName, 0);
				}
			}
		}
		return clazzImplementation;
	}
	
	private <T> T getTypeImplementation(Class<T> clazz) {
		if (clazz.getName().contains("DAO")) {
			return getDAOImplementation(clazz);
		} else {
			return getClassImplementation(clazz);
		}
	}
	
	private boolean set(Object object, String fieldName, Object fieldValue) {
		Class<?> clazz = object.getClass();
		while (clazz != null) {
			try {
				Field field = clazz.getDeclaredField(fieldName);
				field.setAccessible(true);
				field.set(object, fieldValue);
				return true;
			} catch (NoSuchFieldException e) {
				clazz = clazz.getSuperclass();
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		}
		return false;
	}

	public Date newDate() {
		return new Date();
	}

	@SuppressWarnings("unchecked")
	public <T> T getClassImplementation(Class<T> clazz) {
		String jpaPackage = "com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.";
		String className = jpaPackage + clazz.getSimpleName() + "JPAImpl";

		try {
			Class<?> implementationClass = Class.forName(className);
			return (T) implementationClass.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getDAOImplementation(Class<T> clazz){
		String jpaPackage = "com.jjmsoftsolutions.jwebservicesql.dao.impl.";
		String className = jpaPackage + clazz.getSimpleName().replaceAll("DAO", "") + "JPADAOImpl";

		try {
			Class<?> implementationClass = Class.forName(className);
			return (T) implementationClass.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public <T> T cast(Object object, Class<T> clazz) {
		return (T) object;
	}

	@After
	public void tearDown() {
	}

}
