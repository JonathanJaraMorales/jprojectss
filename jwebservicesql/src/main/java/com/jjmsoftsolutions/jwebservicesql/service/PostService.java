package com.jjmsoftsolutions.jwebservicesql.service;

import java.io.UnsupportedEncodingException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.jjmsoftsolutions.jcommon.rest.ServicePathName;

public interface PostService {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(ServicePathName.POST_FIND_BY_ID + "/{id}")
	public Response findById(@PathParam("id") final int id);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(ServicePathName.POST_FIND_BY_LANGUAGE + "/{language}/{begin}/{end}")
	public Response findByLanguage(@PathParam("language") final String language, @PathParam("begin") final int begin, @PathParam("end") final int end);

	@POST
	@Path(ServicePathName.POST_INSERT)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insert(String post);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(ServicePathName.POST_COUNT_BY_LANGUAGE + "/{language}")
	public Response countByLanguage(@PathParam("language") final String language);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(ServicePathName.POST_FIND_BY_TAG + "/{tag}/{begin}/{end}")
	public Response findByTag(@PathParam("tag") final String tag,  @PathParam("begin") final int begin, @PathParam("end") final int end);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(ServicePathName.POST_COUNT_BY_TAG + "/{tag}")
	public Response countByTag(@PathParam("tag") final String tag);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(ServicePathName.POST_FIND_BY_TAG_ID + "/{id}/{begin}/{end}")
	public Response findByTagId(@PathParam("id") final int id,  @PathParam("begin") final int begin, @PathParam("end") final int end);
	
	@DELETE
	@Path(ServicePathName.POST_DELETE+"/{id}")
	public Response deleteByCode(@PathParam("id") final int id);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(ServicePathName.POST_SEARCH_ENGINE + "/{searchEngine}/{begin}/{end}")
	public Response searchEngine(@PathParam("searchEngine") final String searchEngine,  @PathParam("begin") final int begin, @PathParam("end") final int end) throws UnsupportedEncodingException;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(ServicePathName.POST_FIND_BY_LANGUAGE_SUPER_POST + "/{language}/{id}")
	public Response findByLanguageSuperPost(@PathParam("language") final String language,  @PathParam("id") final int id);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(ServicePathName.POST_COUNT_BY_SEARCH_ENGINE + "/{searchEngine}")
	public Response countBySearchEngine(@PathParam("searchEngine") final String searchEngine) throws UnsupportedEncodingException;
}
