/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.model;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.jjmsoftsolutions.jblogger.dao.PostDAO;
import com.jjmsoftsolutions.jblogger.domain.dao.Post;

@Named
public class PostModel {
	
	@Inject private PostDAO dao;
	
	public List<Post> findAllByLanguage(String language, int begin, int end){
		return dao.findAllByLanguage(language, begin, end);
	}

	public Post findById(int id) {
		return dao.findById(id);
	}

	public boolean insert(Post post) {
		return dao.insert(post);
	}
	
	public int countByLanguage(String language){
		return dao.countByLanguage(language);
	}
	
	public int countBySearchEngine(String searchEngine) {
		return dao.countBySearchEngine(searchEngine);
	}

	public List<Post> findAllPostByTag(String tag, int begin, int records) {
		return dao.findAllPostByTag(tag, begin, records);
	}

	public int countByTag(String tag) {
		return dao.countByTag(tag);
	}

	public List<Post> findAllPostByTagId(int id, int begin, int records) {
		return dao.findAllPostByTagId(id, begin, records);
	}

	public boolean delete(int id) {
		return dao.delete(id);
		
	}

	public List<Post> searchEngine(String searchEngine, int begin, int records) {
		return dao.searchEngine(searchEngine, begin, records);
	}

	public Post getPostByLanguage(int id, String languageCode) {
		return dao.findPostByLanguge(id, languageCode);
	}

}
