/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.dao;

import java.util.List;

import com.jjmsoftsolutions.jblogger.domain.dao.Post;

/**
 * Allow to create the specification of Post
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 * @version 1.0
 * @since 1.0
 * @see com.jjmsoftsolutions.jblogger.domain.Parameter
 */
public interface PostDAO {

	public List<Post> findAllByLanguage(String language, int begin, int records);
	public Post findById(int id);
	public boolean insert(Post post);
	public int countByLanguage(String language);	
	public List<Post> findAllPostByTag(String tag, int begin, int records);
	public int countByTag(String tag);
	public List<Post> findAllPostByTagId(int id, int begin, int records);
	public boolean delete(int id);
	public List<Post> searchEngine(String searchEngine, int begin, int records);
	public Post findPostByLanguge(int id, String languageCode);
	public int countBySearchEngine(String searchEngine);

}
