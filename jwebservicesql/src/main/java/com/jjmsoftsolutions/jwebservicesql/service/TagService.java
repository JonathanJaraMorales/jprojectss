package com.jjmsoftsolutions.jwebservicesql.service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.Tag_Translation;

public interface TagService {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findPrincipalTagsByLanguage/{language}")
	public List<Tag_Translation> findPrincipalTagsByLanguage(@PathParam("language") final String language);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findTagsByLanguageAndTitle/{language}/{title}")
	public List<Tag_Translation> findTagsByLanguageAndTitle(
			@PathParam("language") final String language,
			@PathParam("title") final String title);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findTagById/{id}")
	public Tag_Translation findTagById(@PathParam("id") final int id);

}
