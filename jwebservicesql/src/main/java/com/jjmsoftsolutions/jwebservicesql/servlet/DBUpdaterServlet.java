package com.jjmsoftsolutions.jwebservicesql.servlet;

import java.util.List;
import javax.servlet.annotation.WebListener;
import com.jjmsoftsolutions.jcommon.generic.ClassFinder;
import com.jjmsoftsolutions.jdbupdater.update.DataBase;
import com.jjmsoftsolutions.jdbupdater.update.DataBaseImpl;
import com.jjmsoftsolutions.jdbupdater.update.JavaDBUpdater;
import com.jjmsoftsolutions.jdbupdater.update.Update;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.PersistenceProperties;

@WebListener
public class DBUpdaterServlet {

	public DBUpdaterServlet() {
		runUpdates(new JavaDBUpdater(getDataBase()));
	}
	
	private DataBase getDataBase(){
		PersistenceProperties properties = new PersistenceProperties();
		DataBase dataBase = new DataBaseImpl(); 
		dataBase.setDriver(properties.getDriver());
		dataBase.setPassword("Gv-ho?t}~{gn");
		dataBase.setUrl(properties.getUrl());
		dataBase.setUser("ucbdpdbn_updater");
		return dataBase;
	}
	
	private void runUpdates(JavaDBUpdater javaDBUpdater){
		List<Class<?>> classes = ClassFinder.find("com.jjmsoftsolutions.jwebservicesql.update.version");
		for (Class<?> clazz : classes) {
			try {
				Update update = (Update) clazz.newInstance();
				javaDBUpdater.run(update);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
}
