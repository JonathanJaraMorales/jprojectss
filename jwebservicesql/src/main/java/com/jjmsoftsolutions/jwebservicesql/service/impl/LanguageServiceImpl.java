package com.jjmsoftsolutions.jwebservicesql.service.impl;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import com.jjmsoftsolutions.jcommon.exceptions.ConstraintException;
import com.jjmsoftsolutions.jcommon.exceptions.NotFoundException;
import com.jjmsoftsolutions.jcommon.generic.ResponseStatus;
import com.jjmsoftsolutions.jcommon.rest.ServiceName;
import com.jjmsoftsolutions.jwebservicesql.dao.LanguageDAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Language;
import com.jjmsoftsolutions.jwebservicesql.service.LanguageService;
import com.jjmsoftsolutions.jwebservicesql.service.Service;

@Path(ServiceName.LANGUAGE)
@Component
public class LanguageServiceImpl extends Service<Language> implements LanguageService {

	@Inject private LanguageDAO dao;
	
	public Response insert(String jsonRequest) {
		try {
			Language language = cast(Language.class, jsonRequest);
			dao.insert(language);
			return Response.ok().entity(ResponseStatus.OK.getMessage()).type(MediaType.APPLICATION_JSON).build();
		} catch (ConstraintException e) {
			return Response.status(e.getCode()).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
		}
	}
	
	public Response findByLanguageCode(String languageCode) {
		try {
			Language language = dao.findByLanguageCode(languageCode);
			return Response.ok().entity(language).type(MediaType.APPLICATION_JSON).build();
		} catch (NotFoundException e) {
			return Response.status(e.getCode()).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
		}
	}

	
	public Response deleteByCode(String code) {
		try {
			dao.deleteByCode(code);
			return Response.ok().entity(ResponseStatus.OK.getMessage()).type(MediaType.APPLICATION_JSON).build();
		} catch (NotFoundException e) {
			return Response.status(e.getCode()).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
		}
	}

	@Override
	public Response findAll() {
		try {
			List<Language> languages = dao.findAll();
			return Response.ok().entity(languages).type(MediaType.APPLICATION_JSON).build();
		} catch (NotFoundException e) {
			return Response.status(e.getCode()).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
		}
	}

}
