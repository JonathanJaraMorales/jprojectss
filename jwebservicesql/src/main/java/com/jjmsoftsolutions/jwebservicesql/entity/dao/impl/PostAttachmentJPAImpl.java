package com.jjmsoftsolutions.jwebservicesql.entity.dao.impl;

import javax.inject.Named;
import javax.persistence.*;
import org.codehaus.jackson.annotate.JsonBackReference;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Post;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.PostAttachment;

@Named
@Entity
@Table(name="Post_Attachments")
public class PostAttachmentJPAImpl implements PostAttachment {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_Post_Attachments")
	private int id;

	@Column(name="description")
	private String description;

	@Column(name="name")
	private String name;
	
	@Column(name="size")
	private int size;

	@Column(name="type")
	private String type;

	@Lob
	@Column(name="url")
	private String url;
	
	@ManyToOne(cascade= {CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER, targetEntity = PostJPAImpl.class)
	@JoinColumn(name = "id_Post_Translation")
	@JsonBackReference
	private Post post;

	public PostAttachmentJPAImpl() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSize() {
		return this.size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public Post getPost() {
		return post;
	}

	@Override
	public void setPost(Post post) {
		this.post = post;
	}

}