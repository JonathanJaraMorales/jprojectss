/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.jjmsoftsolutions.jblogger.utils;

import java.util.Calendar;
import java.util.Date;

import com.jjmsoftsolutions.jblogger.enums.Locale;

public class DateUtils {

	public static String[] MONTHS_ES= { "Enero", "Febrero", "Marzo",
			"Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre",
			"Octubre", "Noviembre", "Diciembre" };

	public static String[] MONTHS_EN = { "January", "February", "March",
			"April", "May", "June", "July", "August", "September", "October",
			"November", "December" };
	
	public static String[] MONTHS_DE = { "Januar", "Februar", "März",
		"April", "Mai", "Juni", "Juli", "August", "September", "Oktober",
		"November", "Dezember" };

	private static String getMonthLetters(Date date) {
		String locale = new SessionUtils().getLocale();
		if (locale.equals(Locale.ES.getLocale())) {
			return MONTHS_ES[getMonthNumber(date)];
		} else if (locale.equals(Locale.EN.getLocale())) {
			return MONTHS_EN[getMonthNumber(date)];
		} else if (locale.equals(Locale.DE.getLocale())) {
			return MONTHS_DE[getMonthNumber(date)];
		}
		return null;
	}

	public static int getMonthNumber(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MONTH);
	}

	public static int getDayNumber(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	public static int getYear(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.YEAR);
	}

	public static String getDateMMDDYYLetters(Date date) {
		String month = getMonthLetters(date);
		int day = getDayNumber(date);
		int year = getYear(date);
		return month + " " + day + ", " + year;
	}

}
