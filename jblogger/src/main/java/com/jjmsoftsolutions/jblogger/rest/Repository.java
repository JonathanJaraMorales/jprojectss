/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.rest;

import javax.inject.Inject;
import com.jjmsoftsolutions.jblogger.utils.MessageUtils;

public abstract class Repository<T> extends JerseyConnector<T>{
	
	@Inject private MessageUtils message;
	
	public MessageUtils getMessageUtils(){
		return message;
	}
}
