package com.jjmsoftsolutions.jwebservicesql.entity.parser.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import javax.inject.Inject;
import javax.inject.Named;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Language;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Post;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.PostAttachment;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.SuperPost;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Tag;
import com.jjmsoftsolutions.jwebservicesql.entity.parser.dao.PostParser;

@Named
public class PostParserImpl implements PostParser{
	
	@Inject private SuperPost superPost;
	@Inject private Language language;
	@Inject private Post post;
	@Inject private Tag tag;
	@Inject private PostAttachment attachment;
	
	
	public Post jsonToPost(String jsonRequest){
		JsonParser jsonParser = new JsonParser();
		JsonObject jsonObject = (JsonObject)jsonParser.parse(jsonRequest);
		setPost(jsonObject);
		setTags(jsonObject);
		setLanguage(jsonObject);
		setSuperPost(jsonObject);
		setAttachments(jsonObject);
		return post;
	}
	

	private void setPost(JsonObject jsonObject) {
		String content = jsonObject.get("content") != null ? jsonObject.get("content").getAsString() : null;
		String description = jsonObject.get("description")!= null ? jsonObject.get("description").getAsString() : null;
		String title = jsonObject.get("title")!= null ? jsonObject.get("title").getAsString() : null;
		String picture = jsonObject.get("picture")!= null ? jsonObject.get("picture").getAsString() : null;
		
		post.setContent(content);
		post.setCreateDate(new Date());
		post.setUpdateDate(new Date());
		post.setDescription(description);
		post.setTitle(title);
		post.setPicture(picture);
		
	}

	private void setTags(JsonObject jsonObject) {
		JsonArray tagsJson = jsonObject.get("tags") != null ? jsonObject.get("tags").getAsJsonArray() : null;
		if(tagsJson != null){
			post.setTags(new ArrayList<Tag>());
			for(JsonElement element : tagsJson){
				Tag tag = newInstance(this.tag.getClass());
				String name = element.getAsJsonObject().get("name").getAsString();
				tag.setName(name);
				post.addTag(tag);
			}
		}
	}

	private void setLanguage(JsonObject jsonObject) {
		JsonObject languageJson = jsonObject.get("language") != null ? jsonObject.get("language").getAsJsonObject() : null;
		if(languageJson != null){
			String code = languageJson.get("code").getAsString();
			String name = languageJson.get("name").getAsString();	
			language.setCode(code);
			language.setName(name);
			post.setLanguage(language);	
		}
	}

	private void setSuperPost(JsonObject jsonObject){
		if(jsonObject.get("superPost") != null){
			JsonObject superPostJson = jsonObject.get("superPost").getAsJsonObject();
			int idSuperPost = superPostJson.get("id").getAsInt();
			String picture = jsonObject.get("picture").getAsString();
			superPost.setId(idSuperPost);
			superPost.setPicture(picture);
		}
		post.setSuperPost(superPost);
	}
	
	private void setAttachments(JsonObject jsonObject){
		JsonArray attachments = jsonObject.get("attachments") != null ? jsonObject.get("attachments").getAsJsonArray() : null;
		if(attachments != null){
			post.setAttachments(new ArrayList<PostAttachment>());
			for(JsonElement element : attachments){
				PostAttachment attachment = newInstance(this.attachment.getClass());
				String attachmentDescription = element.getAsJsonObject().get("description").getAsString();
				String attachmentName = element.getAsJsonObject().get("name").getAsString();
				String attachmentSize = element.getAsJsonObject().get("size").getAsString();
				String attachmentType = element.getAsJsonObject().get("type").getAsString();
				String attachmentUrl = element.getAsJsonObject().get("url").getAsString();
				attachment.setDescription(attachmentDescription);
				attachment.setName(attachmentName);
				attachment.setSize(new Integer(attachmentSize));
				attachment.setType(attachmentType);
				attachment.setUrl(attachmentUrl);
				post.addAttachment(attachment);
			}
		}
	}
	
	private <T> T newInstance(Class<T> clazz){
		try {
			return (T) clazz.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

}
