package com.jjmsoftsolutions.jblogger.rest;

public class Result<T> {

	private T data;

	public Result(T data) {
		setData(data);
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
