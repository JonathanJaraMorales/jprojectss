package com.jjmsoftsolutions.jblogger.utils;

public interface Localizer {
	public String get(String key);
	public String get(String key, Object... parameters);
}
