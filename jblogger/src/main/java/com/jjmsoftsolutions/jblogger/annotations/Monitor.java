/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.annotations;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jjmsoftsolutions.jblogger.annotations.type.MonitorMemoryType;
import com.jjmsoftsolutions.jblogger.annotations.type.MonitorTimeType;

/***
 * <p>Identifies monitorable methods. A method annotated with {@code @Monitor} that
 * allow to monitor the performance of that method. The information is displayed in the 
 * console and log
 * The {@code @Monitor} has the next parameters:
 * <ul>
 * 	<li>
 * 		{@link com.jjmsoftsolutions.jblogger.annotations.type.MonitorTimeType}
 * 	</li>
 * 	<li>
 * 		{@link com.jjmsoftsolutions.jblogger.annotations.type.MonitorMemoryType}
 * 	</li>
 * </ul>
 * 
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Monitor {
	
	 /**
     * (Optional) The operations that must be monitorable to
     * monitor the elapsed time
     *
     * <p> By default operations are monitorable in seconds.
     */
	MonitorTimeType time() default MonitorTimeType.SECONDS;
	
	 /**
     * (Optional) The operations that must be monitorable to
     * monitor the run time memory
     *
     * <p> By default operations are monitorable in used memory.
     */
	MonitorMemoryType memory() default MonitorMemoryType.USED_MEMORY;
	
	
}