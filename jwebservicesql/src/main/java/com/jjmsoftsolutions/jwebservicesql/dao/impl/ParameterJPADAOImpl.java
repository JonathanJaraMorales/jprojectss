package com.jjmsoftsolutions.jwebservicesql.dao.impl;

import java.util.HashMap;

import javax.inject.Named;

import com.jjmsoftsolutions.jwebservicesql.dao.ParameterDAO;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.JPADAO;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.JPAMetaData;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Parameter;

@Named
public class ParameterJPADAOImpl extends JPADAO<Parameter> implements ParameterDAO {

	public Parameter findByCode(String code) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT P FROM ").append(JPAMetaData.PARAMETER_TABLE_NAME).append(" P ");
		sql.append("WHERE P.code = :code");
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("code", code);
		return find(sql.toString(), parameters);
	}
}
