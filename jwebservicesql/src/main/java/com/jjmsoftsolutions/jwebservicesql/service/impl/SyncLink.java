package com.jjmsoftsolutions.jwebservicesql.service.impl;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

//import javax.faces.bean.ApplicationScoped;
import javax.inject.Named;

//@ApplicationScoped
@Named
public class SyncLink{
    private ReentrantLock lock = new ReentrantLock();
    public Lock getLock(){
       return lock;
    }
}