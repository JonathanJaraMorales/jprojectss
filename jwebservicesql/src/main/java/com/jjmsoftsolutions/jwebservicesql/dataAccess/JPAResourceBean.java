package com.jjmsoftsolutions.jwebservicesql.dataAccess;

import java.io.IOException;
import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.io.InputStream;

@Named
@Singleton
@Deprecated
public class JPAResourceBean {

	private EntityManagerFactory emf = null;
	
	private EntityManager manager;
	
	private String PERSISTENCE_UNIT_NAME = "webservices";
	
	public EntityManagerFactory getFactory() {
		if (emf == null) {
			emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		}
		return emf;
	}
	
	public String getPersistenceUnitName(){
		Properties prop = new Properties();
	
		InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties");  
		try {
			prop.load(input);
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop.getProperty("jdbc.persistence.unitname");
	}

	public EntityManager getEntityManager() {
		return manager;
	}

	public void close() {
		if (emf != null && emf.isOpen()) {
			emf.close();
		}
		emf = null;
	}

}