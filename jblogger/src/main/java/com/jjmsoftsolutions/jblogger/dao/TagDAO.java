/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.dao;

import java.util.List;

import com.jjmsoftsolutions.jblogger.domain.Tag_Translation;

/**
 * Allow to create the specification of TagDao
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 * @version 1.0
 * @since 1.0
 * @see com.jjmsoftsolutions.jblogger.domain.Parameter
 */
public interface TagDAO {
	
	/**
	 * Search a tag translation by language
	 * @param code to search
	 * @return a parameter
	 * @see com.jjmsoftsolutions.jblogger.domain.Tag_Translation
	 */
	public List<Tag_Translation> findTagMenuByLanguage(String language);
	
	/**
	 * Search a tag translation by language and title
	 * @param code to search
	 * @return a parameter
	 * @see com.jjmsoftsolutions.jblogger.domain.Tag_Translation
	 */
	public List<Tag_Translation> findTagsByLanguageAndTitle(String language, String title);
	
	/**
	 * Serat a tag translation by id
	 * @param id to search
	 * @return a tag translation
	 * @see com.jjmsoftsolutions.jblogger.domain.Tag_Translation
	 */
	public Tag_Translation findTagById(int id);
}
