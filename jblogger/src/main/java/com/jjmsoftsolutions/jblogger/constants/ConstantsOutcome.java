package com.jjmsoftsolutions.jblogger.constants;

public class ConstantsOutcome {

	public static final String INDEX = "index";
	public static final String POST_BY_TAG = "postByTag";
	public static final String POST_SEARCH_ENGINE = "engine";
}
