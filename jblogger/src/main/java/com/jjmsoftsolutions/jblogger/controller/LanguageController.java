package com.jjmsoftsolutions.jblogger.controller;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.jjmsoftsolutions.jblogger.domain.dao.Language;
import com.jjmsoftsolutions.jblogger.model.LanguageModel;

@Named("languageController")
public class LanguageController {

	private List<Language> languages;
	@Inject private LanguageModel model;
	
	public List<Language> getAllLanguages(){
		languages = model.findAll();
		return languages;
	}
	
	public List<Language> getLanguages(){
		return languages;
	}
}
