package com.jjmsoftsolutions.jcommon.rest;

public class ServicePathName {
	
	public static final String LANGUAGE_INSERT = "/insert";
	public static final String LANGUAGE_DELETE_BY_CODE = "/deleteByCode";
	public static final String LANGUAGE_FIND_BY_CODE = "/findByCode";
	public static final String LANGUAGE_FIND_ALL = "/findAll";

	public static final String POST_INSERT = "/insert";
	public static final String POST_FIND_BY_ID = "/findById";
	public static final String POST_FIND_BY_LANGUAGE = "/findByLanguage";	
	public static final String POST_FIND_BY_TAG = "/findByTag";
	public static final String POST_FIND_BY_TAG_ID = "/findByTagId";
	public static final String POST_COUNT_BY_TAG= "/countByTag";
	public static final String POST_COUNT_BY_LANGUAGE = "/countByLanguage";
	public static final String POST_DELETE = "/delete";
	public static final String POST_SEARCH_ENGINE = "/searchEngine";
	public static final String POST_FIND_BY_LANGUAGE_SUPER_POST = "/findPostByLanguageSuperPost";
	public static final String POST_COUNT_BY_SEARCH_ENGINE = "/countBySearchEngine";
	
	public static final String PARAMETER_FIND_BY_CODE = "/findByCode";
	public static final String PARAMETER_FIND_ALL = "/findAll";
	public static final String PARAMETER_UPDATE = "/update";
	
	public static final String USER_INSERT = "/insert";
	public static final String USER_FIND_BY_EMAIL_PASSWORD = "/findByEmailPassword";
	
	
}
