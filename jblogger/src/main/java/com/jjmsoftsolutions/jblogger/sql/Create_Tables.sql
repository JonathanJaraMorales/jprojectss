ALTER TABLE Tag
ADD CONSTRAINT FK_Tag_Tag
FOREIGN KEY (id_Tag_Father)
REFERENCES Tag (id_Tag)

ALTER TABLE Tag_Translation
ADD CONSTRAINT FK_Tag_Translation_Tag_Translation
FOREIGN KEY (id_Tag_Translation_Father)
REFERENCES Tag_Translation (id_Tag_Translation)

ALTER TABLE  Post DROP FOREIGN KEY FK_Post_Tag
ALTER TABLE  Tag_Translation DROP FOREIGN KEY FK_Tag_Translation_Tag