package com.jjmsoftsolutions.jwebservicesql.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.jjmsoftsolutions.jwebservicesql.dao.TagTranslationDao;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.Tag_Translation;
import com.jjmsoftsolutions.jwebservicesql.service.TagService;
@Path("/tag")
@Component
public class TagServiceImpl implements TagService{
	
	@Inject private TagTranslationDao dao;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findPrincipalTagsByLanguage/{language}")
	public List<Tag_Translation> findPrincipalTagsByLanguage(@PathParam("language") final String language){
		return dao.findPrincipalTagsByLanguage(language);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findTagsByLanguageAndTitle/{language}/{title}")
	public List<Tag_Translation> findTagsByLanguageAndTitle(
			@PathParam("language") final String language,
			@PathParam("title") final String title){
		List<Tag_Translation> tags = dao.findTagsByLanguageAndTitle(language, title);
		List<Tag_Translation> toReturn = new ArrayList<Tag_Translation>();
		
		for(Tag_Translation tag: tags){
			tag.setTags(null);
			toReturn.add(tag);
		}
		
		return toReturn;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findTagById/{id}")
	public Tag_Translation findTagById(@PathParam("id") final int id){
		return dao.findTagById(id);
	}
	
}
