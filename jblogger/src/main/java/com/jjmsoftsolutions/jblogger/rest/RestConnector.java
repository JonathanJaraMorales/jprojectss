/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.rest;

import java.util.List;


public interface RestConnector<E> {
	public E find(String pathService, String pathMethod, Object... param);
	public List<E> findAll(String pathService, String pathMethod, Object... param);
	public boolean insert(String pathService, String pathMethod, Object object);
	public int count(String pathService, String pathMethod, Object... param);
	public boolean delete(String pathService, String pathMethod, Object object);
	
}
