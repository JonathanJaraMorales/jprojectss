package com.jjmsoftsolutions.jcommon.exceptions;

public class EntityValidatorException extends JException {

	private static final long serialVersionUID = 1L;
	
	public EntityValidatorException(int code, String message) {
		super(code, message);
	}
		
}
