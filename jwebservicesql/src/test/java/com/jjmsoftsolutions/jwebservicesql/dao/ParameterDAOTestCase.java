package com.jjmsoftsolutions.jwebservicesql.dao;

import org.junit.Assert;
import org.junit.Test;

import com.jjmsoftsolutions.jcommon.exceptions.NotFoundException;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.GeneralDAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Parameter;
import com.jjmsoftsolutions.jwebservicesql.test.factory.JPATestFactory;

public class ParameterDAOTestCase extends JPATestFactory {

	private ParameterDAO dao;
	
	@Override
	public GeneralDAO<?> getConfig() {
		dao = newInstance(ParameterDAO.class);
		return dao;
	}
	
	@Test
	public void findByCode_canFindParameter_ExpectedParemeter() {
		Parameter parameter = newInstance(Parameter.class);
		parameter.setCode("PARAMETER_CODE");
		parameter.setContent("PARAMETER CONTENT");
		buildAndSave(parameter);
		Assert.assertEquals(parameter.getCode(), dao.findByCode(parameter.getCode()).getCode());
	}
	
	//@Test
	public void findByCode_cannotFindUnregisterParameter_ExpectedNoResultException() {
		Parameter parameter = newInstance(Parameter.class);
		parameter.setCode("PARAMETER_CODE");
		parameter.setContent("PARAMETER CONTENT");
		exception.expect(NotFoundException.class);
		dao.findByCode(parameter.getCode());
	}

}
