/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import com.jjmsoftsolutions.jblogger.domain.Parameter;
import com.jjmsoftsolutions.jblogger.model.ParameterModel;
import com.jjmsoftsolutions.jblogger.utils.Localizer;

/**
 * Allows to control the Parameters in the application
 * 
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@Named("parameterController")
@ViewScoped
@Singleton
public class ParameterController implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject private ParameterModel model;
	@Inject private Localizer localizer;
	
	private List<Parameter> parameters;
	private Map<String, Parameter> map;
	private Map<Parameter,Boolean> checkMap = new HashMap<Parameter,Boolean>();
	private Map<String, String> original;
	private String tab;
	
	public void setTab(String tab){
		this.tab = tab;
	}
	
	public String getTab(){
		return tab;
	}
	
	public void setParameterTab(String tab){
		setTab(tab);
	}


	public Parameter getParameterByCode(String parameter) {
		if(map == null || parameters == null || map.size() == 0 || parameters.size() == 0){
			getAllParameter();
		}
		return map.get(parameter);
	}

	public List<Parameter> getParameters() {
		return parameters;
	}

	public List<Parameter> getAllParameter() {
		if(map == null || parameters == null || map.size() == 0 || parameters.size() == 0){
			parameters = model.findAll();
			map = new HashMap<String, Parameter>();
			original = new HashMap<String, String>();
			for(Parameter parameter : parameters){
				map.put(parameter.getCode(), parameter);
				original.put(parameter.getCode(), parameter.getContent());
			}
		}
		return parameters;
	}
	
	public void save(){
		for (String key: getMap().keySet()) {
			if(!getParameterByCode(key).getContent().trim().equals(original.get(key).trim())) {
				model.update(getParameterByCode(key));
			}
		}
		map.clear();
		parameters.clear();
	}
	
	public Map<Parameter, Boolean> getCheckMap() {
		if(checkMap.size() == 0){
			for (String key: getMap().keySet()) {
				if(key.contains("POST_INFO_")){
					checkMap.put(map.get(key), map.get(key).getContent().equals("1"));
				}
			}
		}
		return checkMap;
	}
	
	public void saveCheckBox(){
		boolean isSaving = false;
		for (Parameter parameter: getCheckMap().keySet()) {
			String content = getCheckMap().get(parameter) == true ? "1" : "2";
			if(!content.equals(getParameterByCode(parameter.getCode()).getContent())) {
				isSaving = true;
				parameter.setContent(content);
				model.update(parameter);
			}
		}
		if(isSaving){
			checkMap.clear();
			map.clear();
			parameters.clear();
		}
	}
	
	public String getLocalizationMessage(String key){
		return localizer.get(key);
	}
	
	private Map<String, Parameter> getMap(){
		if(map.size() == 0){
			getAllParameter();
		}
		return map;
	}
	
}
