/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.annotations.type;

/**
* Type used to indicate a specific mapping of {@code com.jjmsoftsolutions.website.annotation.Monitor} to validate the
* time type evaluation
* 
* @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
* @version 1.0
* @since 1.0
*/
public enum MonitorTimeType {
	
	/**
	 * Show the elapsed time in Nanoseconds
	 */
	NANO_SECONDS,
	
	/**
	 * Show the elapsed time in seconds
	 */
	SECONDS,
	
	/**
	 * Show the elapsed time in minutes  
	 */
	MINUTES, 
}
