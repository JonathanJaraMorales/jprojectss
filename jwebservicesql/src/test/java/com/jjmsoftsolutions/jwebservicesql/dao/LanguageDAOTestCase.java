package com.jjmsoftsolutions.jwebservicesql.dao;

import org.junit.Assert;
import org.junit.Test;
import com.jjmsoftsolutions.jcommon.exceptions.NotFoundException;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.GeneralDAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Language;
import com.jjmsoftsolutions.jwebservicesql.test.factory.JPATestFactory;

public class LanguageDAOTestCase extends JPATestFactory {

	private LanguageDAO dao;

	@Override
	public GeneralDAO<?> getConfig() {
		dao = newInstance(LanguageDAO.class);
		return dao;
	}
	
	@Test
	public void findByLanguageCode_canFindLanguageByCode_ExpectedLanguage() {
		Language language = newInstance(Language.class);
		language.setCode("EN");
		buildAndSave(language);
		Assert.assertEquals("ENS", dao.findByLanguageCode(language.getCode()).getCode());	
	}
	
	@Test
	public void findByLanguageCode_cannotFindByUnregisterLanguage_ExpectedNoResultException() {
		exception.expect(NotFoundException.class);
		dao.findByLanguageCode("EN").getCode();
	}

}
