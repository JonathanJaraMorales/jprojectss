/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.jjmsoftsolutions.jblogger.dao.impl;

import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.inject.Named;
import com.jjmsoftsolutions.jblogger.dao.PostDAO;
import com.jjmsoftsolutions.jblogger.domain.dao.Post;
import com.jjmsoftsolutions.jblogger.rest.Repository;
import com.jjmsoftsolutions.jcommon.exceptions.NotFoundException;
import com.jjmsoftsolutions.jcommon.rest.ServiceName;
import com.jjmsoftsolutions.jcommon.rest.ServicePathName;

/**
 * Allow to create the implementation of {@code Post_Translation}
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 * @version 1.0
 * @since 1.0
 * @see com.jjmsoftsolutions.jblogger.domain.Post_Translation
 * @see com.jjmsoftsolutions.jblogger.dao.PostDAO
 * @see com.jjmsoftsolutions.jblogger.rest.Repository
 */
@Named
public class PostDAOImpl extends Repository<Post> implements PostDAO {

	public List<Post> findAllByLanguage(String language, int begin, int end) {
		return findAll(ServiceName.POST, ServicePathName.POST_FIND_BY_LANGUAGE, language, begin, end);
	}

	public Post findById(int id) {
		return find(ServiceName.POST, ServicePathName.POST_FIND_BY_ID, id);
	}

	public boolean insert(Post post) {
		try {
			insert(ServiceName.POST, ServicePathName.POST_INSERT, post);
			getMessageUtils().addSuccessLocalizerMessageToPage("Info.Post.Save", post.getTitle());
			return true;
		} catch (NotFoundException e) {
			getMessageUtils().addErrorLocalizerMessageToPage("Error.Post.Save",  post.getTitle());
			return false;
		}
	}
	
	public int countByLanguage(String language){
		return count(ServiceName.POST, ServicePathName.POST_COUNT_BY_LANGUAGE, language);
	}

	public List<Post> findAllPostByTag(String tag, int begin, int records) {
		return findAll(ServiceName.POST, ServicePathName.POST_FIND_BY_TAG, tag, begin, records);
	}

	public int countByTag(String tag) {
		return count(ServiceName.POST, ServicePathName.POST_COUNT_BY_TAG, tag);
	}

	public List<Post> findAllPostByTagId(int id, int begin, int records) {
		return findAll(ServiceName.POST, ServicePathName.POST_FIND_BY_TAG_ID, id, begin, records);
	}

	public boolean delete(int id) {
		Post post = findById(id);
		try {
			delete(ServiceName.POST, ServicePathName.POST_DELETE, id);
			getMessageUtils().addSuccessLocalizerMessageToPage("Info.Post.Delete", post.getTitle());
			return true;
		} catch (NotFoundException e) {
			getMessageUtils().addErrorLocalizerMessageToPage("Error.Post.Delete",  post.getTitle());
			return false;
		}
	}

	public List<Post> searchEngine(String searchEngine, int begin, int records) {
		try {
			List<Post> list = findAll(ServiceName.POST, ServicePathName.POST_SEARCH_ENGINE, searchEngine, begin, records);
			int count = countBySearchEngine(searchEngine);
			byte ptext[] = searchEngine.getBytes("ISO-8859-1"); 
			getMessageUtils().addSuccessLocalizerMessageToPage("Info.Search.Engine", count, new String(ptext, "UTF-8"));
			return list;
		} catch (NotFoundException e) {
			getMessageUtils().addErrorLocalizerMessageToPage("Error.Search.Engine",  searchEngine);
			return null;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Post findPostByLanguge(int id, String languageCode) {
		return find(ServiceName.POST, ServicePathName.POST_FIND_BY_LANGUAGE_SUPER_POST, languageCode, id);
	}

	@Override
	public int countBySearchEngine(String searchEngine) {
		return count(ServiceName.POST, ServicePathName.POST_COUNT_BY_SEARCH_ENGINE, searchEngine);
	}
}
