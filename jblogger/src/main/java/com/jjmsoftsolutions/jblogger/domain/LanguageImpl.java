package com.jjmsoftsolutions.jblogger.domain;

import com.jjmsoftsolutions.jblogger.domain.dao.Language;
import com.jjmsoftsolutions.jblogger.domain.dao.Post;

import java.util.List;

import javax.inject.Named;

import org.codehaus.jackson.annotate.JsonIgnore;

@Named
public class LanguageImpl implements Language {

	private static final long serialVersionUID = 1L;

	private String code;
	private String name;
	private List<Post> post;
	private int enable;
	
	public LanguageImpl() {
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Post> getPost() {
		return this.post;
	}

	public void setPost(List<Post> post) {
		this.post = post;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public int getEnable() {
		return enable;
	}

	@Override
	public void setEnable(int enable) {
		this.enable = enable;
	}

}