package com.jjmsoftsolutions.jblogger.domain;

import java.io.Serializable;
import java.util.List;

public class Tag_Translation implements Serializable {
	private static final long serialVersionUID = 1L;

	
	private int id_Tag_Translation;


	private LanguageImpl language;

	private String title;


	private List<Tag_Translation> tags;
	

	private Tag_Translation id_Tag_Translation_Father;
	

	public Tag_Translation getId_Tag_Translation_Father() {
		return id_Tag_Translation_Father;
	}

	public void setId_Tag_Translation_Father(
			Tag_Translation id_Tag_Translation_Father) {
		this.id_Tag_Translation_Father = id_Tag_Translation_Father;
	}

	public List<Tag_Translation> getTags() {
		return tags;
	}

	public void setTags(List<Tag_Translation> tags) {
		this.tags = tags;
	}

	public Tag_Translation() {
	}

	public int getId_Tag_Translation() {
		return this.id_Tag_Translation;
	}

	public void setId_Tag_Translation(int id_Tag_Translation) {
		this.id_Tag_Translation = id_Tag_Translation;
	}

	public LanguageImpl getLanguage() {
		return this.language;
	}

	public void setLanguage(LanguageImpl language) {
		this.language = language;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}



}