/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.jjmsoftsolutions.jblogger.model;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.jjmsoftsolutions.jblogger.dao.ParameterDAO;
import com.jjmsoftsolutions.jblogger.domain.Parameter;

@Named
public class ParameterModel {
	
	@Inject private ParameterDAO dao;
	
	public Parameter findByCode(String parameter){
		return dao.findByCode(parameter);
	}

	public List<Parameter> findAll() {
		return dao.findAll();
	}

	public void update(Parameter parameter) {
		dao.update(parameter);
	}

}
