package com.jjmsoftsolutions.jblogger.domain;

import java.io.Serializable;

/**
 * The persistent class for the Parameter database table.
 * 
 */

public class Parameter implements Serializable {
	private static final long serialVersionUID = 1L;

	
	private String code;

	
	private String content;

	private String language_Code;

	public Parameter() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getLanguage_Code() {
		return this.language_Code;
	}

	public void setLanguage_Code(String language_Code) {
		this.language_Code = language_Code;
	}

}