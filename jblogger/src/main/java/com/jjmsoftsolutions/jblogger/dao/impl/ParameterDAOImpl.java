/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.jjmsoftsolutions.jblogger.dao.impl;

import java.util.List;

import javax.inject.Named;

import com.jjmsoftsolutions.jblogger.dao.ParameterDAO;
import com.jjmsoftsolutions.jblogger.domain.Parameter;
import com.jjmsoftsolutions.jblogger.rest.Repository;
import com.jjmsoftsolutions.jcommon.rest.ServiceName;
import com.jjmsoftsolutions.jcommon.rest.ServicePathName;

/**
 * Allow to create the implementation of {@code ParameterDao}
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 * @version 1.0
 * @since 1.0
 * @see com.jjmsoftsolutions.jblogger.domain.Parameter
 * @see com.jjmsoftsolutions.jblogger.dao.ParameterDAO
 * @see com.jjmsoftsolutions.jblogger.rest.Repository
 */
@Named
public class ParameterDAOImpl extends Repository<Parameter> implements ParameterDAO {

	public Parameter findByCode(String code) {
		return find(ServiceName.PARAMETER, ServicePathName.PARAMETER_FIND_BY_CODE, code);
	}

	public List<Parameter> findAll() {
		return findAll(ServiceName.PARAMETER, ServicePathName.PARAMETER_FIND_ALL);
	}

	public void update(Parameter parameter) {
		update(ServiceName.PARAMETER, ServicePathName.PARAMETER_UPDATE, parameter);
	}

}
