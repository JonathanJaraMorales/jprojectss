package com.jjmsoftsolutions.jblogger.utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

import javax.inject.Inject;
import javax.inject.Named;

import static java.util.logging.Logger.getLogger;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import com.jjmsoftsolutions.jblogger.controller.ParameterController;

import static org.apache.commons.net.ftp.FTPReply.isPositiveCompletion;

@Named
public class FTPUtils {
	
	//@Inject private ParameterController parameterController;
	
	private String user;
	private String password;
	private String host;
	
	public FTPUtils(){
		
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}
		
	public boolean upload(String directory, InputStream inputStream, String fileName) {
		try {
			FTPClient ftpClient = new FTPClient();
			ftpClient.connect(host);
			ftpClient.login(user, password);
			if (!isPositiveCompletion(ftpClient.getReplyCode())) {
				ftpClient.disconnect();
			} else {
				ftpClient.changeWorkingDirectory(directory);
				ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
				BufferedInputStream buffIn = new BufferedInputStream(inputStream);
				ftpClient.enterLocalPassiveMode();
				boolean res = ftpClient.storeFile(fileName, buffIn);
				buffIn.close();
				ftpClient.logout();
				ftpClient.disconnect();
				return res;
			}
		} catch (IOException ex) {
			getLogger(FTPUtils.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}

	public boolean createFolder(String directory, String folderName) {
		try {
			FTPClient ftpClient = new FTPClient();
			ftpClient.connect(host);
			ftpClient.login(user, password);
			if (!isPositiveCompletion(ftpClient.getReplyCode())) {
				ftpClient.disconnect();
			} else {
				ftpClient.changeWorkingDirectory(directory);
				ftpClient.makeDirectory(folderName);
			}
		} catch (IOException ex) {
			getLogger(FTPUtils.class.getName()).log(Level.SEVERE, null, ex);
		}
		return false;
	}
}
