package com.jjmsoftsolutions.jblogger.domain;

import com.jjmsoftsolutions.jblogger.domain.dao.Post;
import com.jjmsoftsolutions.jblogger.domain.dao.Tag;

public class TagImpl implements Tag {
	
	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private Post post;

	public TagImpl() {
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
		
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	


}