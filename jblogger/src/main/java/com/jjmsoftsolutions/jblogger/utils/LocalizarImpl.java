package com.jjmsoftsolutions.jblogger.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.inject.Singleton;

@Named
@Singleton
@SessionScoped
public class LocalizarImpl implements Localizer {

	protected static final Control UTF8_CONTROL = new UTF8Control();
	protected static final String BUNDLE_EXTENSION = "properties";

	@Override
	public String get(String key) {
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		ResourceBundle resourceBundle = ResourceBundle.getBundle("com.jjmsoftsolutions.messages", locale, UTF8_CONTROL);
		return resourceBundle.getString(key);
	}

	@Override
	public String get(String key, Object... parameters) {
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		return MessageFormat.format(ResourceBundle.getBundle("com.jjmsoftsolutions.messages", locale).getString(key), parameters);
	}
	
	  protected static class UTF8Control extends Control {
	        public ResourceBundle newBundle
	            (String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
	                throws IllegalAccessException, InstantiationException, IOException
	        {
	            // The below code is copied from default Control#newBundle() implementation.
	            // Only the PropertyResourceBundle line is changed to read the file as UTF-8.
	            String bundleName = toBundleName(baseName, locale);
	            String resourceName = toResourceName(bundleName, BUNDLE_EXTENSION);
	            ResourceBundle bundle = null;
	            InputStream stream = null;
	            if (reload) {
	                URL url = loader.getResource(resourceName);
	                if (url != null) {
	                    URLConnection connection = url.openConnection();
	                    if (connection != null) {
	                        connection.setUseCaches(false);
	                        stream = connection.getInputStream();
	                    }
	                }
	            } else {
	                stream = loader.getResourceAsStream(resourceName);
	            }
	            if (stream != null) {
	                try {
	                    bundle = new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"));
	                } finally {
	                    stream.close();
	                }
	            }
	            return bundle;
	        }
	    }

}
