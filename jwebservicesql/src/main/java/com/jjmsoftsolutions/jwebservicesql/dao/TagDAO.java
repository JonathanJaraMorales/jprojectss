package com.jjmsoftsolutions.jwebservicesql.dao;

import java.util.List;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.GeneralDAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Tag;

public interface TagDAO extends GeneralDAO<Tag> {
	public List<Tag> findTagsByName(String tag, int begin, int end);
	public Integer countByName(String name);
}
