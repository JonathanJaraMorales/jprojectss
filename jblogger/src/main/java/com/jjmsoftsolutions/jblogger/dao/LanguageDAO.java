package com.jjmsoftsolutions.jblogger.dao;

import java.util.List;

import com.jjmsoftsolutions.jblogger.domain.dao.Language;

public interface LanguageDAO {

	public Language findByCode(String code);
	public List<Language> findAll();
}
