/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;

import com.jjmsoftsolutions.jblogger.converter.TagConverter;
import com.jjmsoftsolutions.jblogger.domain.Tag_Translation;
import com.jjmsoftsolutions.jblogger.model.TagModel;

/**
 * Allows to control the Post in the application
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@Named("tagController")
@ViewScoped
public class TagController implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject private TagModel model;
	@Inject private LocaleController controller;
	@Inject private TagConverter converter;
	
	private List<Tag_Translation> selectedTags = new ArrayList<Tag_Translation>();
	private List<Tag_Translation> allTags = new ArrayList<Tag_Translation>();
	
	/**
	 * Search the Tags Translations by language to build the principal menu
	 * @return a list of tag translation
	 * @see java.util.List;
	 * @see com.jjmsoftsolutions.jblogger.domain.Tag_Translation
	 */
	public List<Tag_Translation> getTagMenu(){
		String language = controller.getLocale().toUpperCase();
		return model.getTagMenuByLanguage(language);
	}
	
	/**
	 * Search the tags by language to create an autocomplete primefaces component
	 * @param query a string to load the list
	 * @return a list of tag translation
	 * @see java.util.List
	 * @see com.jjmsoftsolutions.jblogger.domain.Tag_Translation
	 */
	public List<Tag_Translation> completeTags(String query) {
		String language = controller.getLocale().toUpperCase();
		List<Tag_Translation> temporalTags = model.findTagsByLanguageAndTitle(language, query);
	    List<Tag_Translation> fiteredTags = new ArrayList<Tag_Translation>();   
	    for(Tag_Translation tag: temporalTags){
	    	if(tag.getTitle().toLowerCase().contains(query.toLowerCase())){
	    		fiteredTags.add(tag);
	    	}
	    }
	    converter.setTemporalTags(temporalTags);
	    return fiteredTags;
	}

	/**
	 * Return a list of selected tags
	 * @return a list of selected tags
	 * @see java.util.List
	 * @see com.jjmsoftsolutions.jblogger.domain.Tag_Translation
	 */
	public List<Tag_Translation> getSelectedTags() {
		allTags.addAll(selectedTags);
		return selectedTags;
	}

	public List<Tag_Translation> getAllTags() {
		return allTags;
	}

	public void setAllTags(List<Tag_Translation> allTags) {
		this.allTags = allTags;
	}

	/**
	 * Set a list of selected tags
	 * @param selectedTags a list of selected tags
	 * @see java.util.List
	 * @see com.jjmsoftsolutions.jblogger.domain.Tag_Translation
	 */
	public void setSelectedTags(List<Tag_Translation> selectedTags) {
		this.selectedTags = selectedTags;
	}
	
	public void handleSelect(SelectEvent e){
		System.out.print("");
	}
	public void handleUnSelect(SelectEvent e){}

	public TagConverter getConverter() {
		return converter;
	}

	public void setConverter(TagConverter converter) {
		this.converter = converter;
	}

}
