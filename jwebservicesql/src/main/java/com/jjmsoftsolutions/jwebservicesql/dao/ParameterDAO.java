package com.jjmsoftsolutions.jwebservicesql.dao;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Parameter;
import com.jjmsoftsolutions.jwebservicesql.dataAccess.GeneralDAO;

public interface ParameterDAO extends GeneralDAO<Parameter> {
	public Parameter findByCode(String code);
}
