package com.jjmsoftsolutions.jblogger.domain.dao;

import java.io.Serializable;

import javax.servlet.http.Part;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.jjmsoftsolutions.jblogger.domain.PostAttachmentImpl;

@JsonDeserialize(as = PostAttachmentImpl.class)
public interface PostAttachment extends Serializable{
	public int getId();
	public void setId(int id);
	
	public String getDescription();
	public void setDescription(String description);
	
	public String getName();
	public void setName(String name);
	
	public int getSize();
	public void setSize(int size);

	public String getType();
	public void setType(String type);

	public String getUrl();
	public void setUrl(String url);
	
	public Post getPost();
	public void setPost(Post post);
	
	public void setPart(Part part);
	public Part getPart();
}

