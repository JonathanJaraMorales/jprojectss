package com.jjmsoftsolutions.jblogger.model;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.jjmsoftsolutions.jblogger.dao.LanguageDAO;
import com.jjmsoftsolutions.jblogger.domain.dao.Language;

@Named
public class LanguageModel {
	
	@Inject private LanguageDAO dao;

	public Language findByCode(String code) {
		return dao.findByCode(code);
	}

	public List<Language> findAll() {
		return dao.findAll();
	}

}
