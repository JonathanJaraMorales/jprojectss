-- -----------------------------------------------------
-- Table Language
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Language (
  language_Code VARCHAR(2) NOT NULL,
  name VARCHAR(25) NOT NULL,
  enable INT NULL,
  PRIMARY KEY (language_Code))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Post
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Post (
  id_Post INT NOT NULL AUTO_INCREMENT,
  picture LONGTEXT NOT NULL,
  PRIMARY KEY (id_Post))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table User
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS User (
  email VARCHAR(150) NOT NULL,
  password VARCHAR(150) NOT NULL,
  created_Date DATETIME NOT NULL,
  nick_Name VARCHAR(25) NOT NULL,
  photo LONGTEXT NULL,
  available CHAR NOT NULL,
  PRIMARY KEY (email))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Post_Activity
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Post_Activity (
  id_Post_Activity INT NOT NULL,
  email VARCHAR(150) NOT NULL,
  PRIMARY KEY (id_Post_Activity),
  INDEX FK_Post_Acticity_User_idx (email ASC),
  CONSTRAINT FK_Post_Acticity_User
    FOREIGN KEY (email)
    REFERENCES User (email)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Tag_Translation
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Tag_Translation (
  id_Tag_Translation INT NOT NULL AUTO_INCREMENT,
  language_Code VARCHAR(2) NOT NULL,
  title VARCHAR(25) NOT NULL,
  id_Tag_Translation_Father INT NULL,
  PRIMARY KEY (id_Tag_Translation),
  INDEX FK_Tag_Translation_Tag_Translation_idx (id_Tag_Translation_Father ASC),
  CONSTRAINT FK_Tag_Translation_Language
    FOREIGN KEY (language_Code)
    REFERENCES Language (language_Code)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT FK_Tag_Translation_Tag_Translation
    FOREIGN KEY (id_Tag_Translation_Father)
    REFERENCES Tag_Translation (id_Tag_Translation)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Post_Comment
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Post_Comment (
  id_Post_Comment INT NOT NULL,
  id_Post_Activity INT NOT NULL,
  id_Post_CommentFather INT NOT NULL,
  comment LONGTEXT NOT NULL,
  created_Date DATETIME NOT NULL,
  updated_Date DATETIME NOT NULL,
  PRIMARY KEY (id_Post_Comment),
  INDEX FK_Post_Comment_Post_Comment_idx (id_Post_CommentFather ASC),
  INDEX FK_Post_Comment_Post_Activity_idx (id_Post_Activity ASC),
  CONSTRAINT FK_Post_Comment_Post_Comment
    FOREIGN KEY (id_Post_CommentFather)
    REFERENCES Post_Comment (id_Post_Comment)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT FK_Post_Comment_Post_Activity
    FOREIGN KEY (id_Post_Activity)
    REFERENCES Post_Activity (id_Post_Activity)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Post_Translation
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Post_Translation (
  id_Post_Translation INT NOT NULL AUTO_INCREMENT,
  id_Language VARCHAR(2) NOT NULL,
  id_Post INT NOT NULL,
  title VARCHAR(150) NOT NULL,
  description LONGTEXT NOT NULL,
  content LONGTEXT NOT NULL,
  create_Date DATETIME NOT NULL,
  update_Date DATETIME NOT NULL,
  picture LONGTEXT NOT NULL,
  PRIMARY KEY (id_Post_Translation),
  INDEX FK_Post_Translation_Language_idx (id_Language ASC),
  INDEX FK_Post_Translation_Post_idx (id_Post ASC),
  CONSTRAINT FK_Post_Translation_Language
    FOREIGN KEY (id_Language)
    REFERENCES Language (language_Code)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT FK_Post_Translation_Post
    FOREIGN KEY (id_Post)
    REFERENCES Post (id_Post)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
PACK_KEYS = Default;


-- -----------------------------------------------------
-- Table Parameter
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Parameter (
  code VARCHAR(100) NOT NULL,
  content LONGTEXT NOT NULL,
  PRIMARY KEY (code))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Menu
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Menu (
  id_Menu_Category INT NOT NULL,
  title VARCHAR(50) NULL,
  PRIMARY KEY (id_Menu_Category))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Post_Translations_Tags
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Post_Translations_Tags (
  idPost_Translations_Tags INT NOT NULL AUTO_INCREMENT,
  id_Post_Translation INT NULL,
  id_Tag_Translation INT NOT NULL,
  PRIMARY KEY (idPost_Translations_Tags),
  INDEX FK_Post_Translations_Tags_Post_Translations_idx (id_Post_Translation ASC),
  INDEX FK_Post_Translations_Tags_Tags_Translation_idx (id_Tag_Translation ASC),
  CONSTRAINT FK_Post_Translations_Tags_Post_Translations
    FOREIGN KEY (id_Post_Translation)
    REFERENCES Post_Translation (id_Post_Translation)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT FK_Post_Translations_Tags_Tags_Translation
    FOREIGN KEY (id_Tag_Translation)
    REFERENCES Tag_Translation (id_Tag_Translation)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table Tag
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Tag (
  id_Tag INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  id_Post_Translation INT NOT NULL,
  PRIMARY KEY (id_Tag),
  INDEX FK_Post_Translation_Tag_idx (id_Post_Translation ASC),
  CONSTRAINT FK_Post_Translation_Tag
    FOREIGN KEY (id_Post_Translation)
    REFERENCES Post_Translation (id_Post_Translation)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
