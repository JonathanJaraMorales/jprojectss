package com.jjmsoftsolutions.jblogger.domain.dao;

import java.io.Serializable;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import com.jjmsoftsolutions.jblogger.domain.PostVideoImpl;

@JsonDeserialize(as=PostVideoImpl.class)
public interface PostVideo extends Serializable {

	public int getId();
	public void setId(int id);
	
	public String getCreatedDate();
	public void setCreatedDate(String createdDate);
	
	public SuperPost getSuperPost();
	public void setSuperPost(SuperPost post);

	public String getType();
	public void setType(String type);

	public String getUrl();
	public void setUrl(String url);
}
