package com.jjmsoftsolutions.jwebservicesql.dataAccess;

import java.util.List;
import java.util.Map;
import org.springframework.core.GenericTypeResolver;

@Deprecated
public abstract class Repository<T> {
	
	private JPAConnector<T> connector;

	@SuppressWarnings("unchecked")
	public Class<T> getEntityClass() {
		return (Class<T>) GenericTypeResolver.resolveTypeArguments(getClass(), Repository.class)[0];
	}
	
	public T findById(int id){
		return connector.findById(id, getEntityClass());
	}
	
	public List<T> findAll(){
		return connector.findAll(getEntityClass());
	}
	
	public List<T> findAll(String sql, Map<String, Object> parameters){
		return connector.findAll(sql, parameters, getEntityClass());
	}
	
	public List<T> findAll(String sql, Map<String, Object> parameters, int begin, int end){
		return connector.findAll(sql, parameters, getEntityClass(), begin, end);
	}
	
	public T find(String sql, Map<String, Object> parameters){
		return connector.find(sql, parameters, getEntityClass());
	}
	
	public boolean insert(T object){
		return connector.insert(object);
	}
	
	public T doInsert(T object){
		return connector.doInsert(object);
	}
	
	public int count(String sql, Map<String, Object> parameters){
		return connector.count(sql, parameters, getEntityClass());
	}
	
}
