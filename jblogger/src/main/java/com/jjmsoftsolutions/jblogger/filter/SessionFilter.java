package com.jjmsoftsolutions.jblogger.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jjmsoftsolutions.jblogger.controller.LoginController;

@WebFilter(filterName = "SessionFilter", urlPatterns = {"/faces/settings.xhtml", "/faces/post.xhtml", "/faces/postDetailEdit.xhtml"})
public class SessionFilter implements Filter {

	private FilterConfig filterConfig = null;
	 
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		try {
			HttpServletRequest req = (HttpServletRequest) request;
	        LoginController session = (LoginController) req.getSession().getAttribute("loginController");
	        if (session != null && session.isLogged()) {
	            chain.doFilter(request, response);
	        } else{
	            HttpServletResponse res = (HttpServletResponse) response;
	            res.sendRedirect(req.getContextPath() + "/home");
	        }
	    } catch (IOException t) {
	        System.out.println(t.getMessage());
	    } catch (ServletException t) {
	        System.out.println(t.getMessage());
	    }
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	
	 public FilterConfig getFilterConfig() {
		 return (this.filterConfig);
	 }

}
