package com.jjmsoftsolutions.jblogger.constants;

public class ConstantsParam {
	
	public static final String TAG_PARAM = "tag";
	public static final String SIZE_PARAM = "size";
	public static final String ID_PARAM = "id";
	public static final String SEARCH_PARAM = "search";

}
