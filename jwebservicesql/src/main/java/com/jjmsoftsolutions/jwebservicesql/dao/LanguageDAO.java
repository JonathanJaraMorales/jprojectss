package com.jjmsoftsolutions.jwebservicesql.dao;

import com.jjmsoftsolutions.jwebservicesql.dataAccess.GeneralDAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Language;

public interface LanguageDAO extends GeneralDAO<Language> {
	public Language findByLanguageCode(String languageCode);
	public void deleteByCode(String code);
}
