package com.jjmsoftsolutions.jwebservicesql.entity.dao;

import java.io.Serializable;
import java.util.List;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.LanguageJPAImpl;

@JsonDeserialize(as = LanguageJPAImpl.class)
public interface Language extends Serializable{
	public String getCode();
	public void setCode(String code);
	public int getEnable();
	public void setEnable(int enable);
	public String getName();
	public void setName(String name);
	public List<Post> getPost();
	public void setPost(List<Post> postTranslations);
}
