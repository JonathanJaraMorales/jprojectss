package com.jjmsoftsolutions.jblogger.dao.impl;

import java.util.List;

import javax.inject.Named;

import com.jjmsoftsolutions.jblogger.dao.LanguageDAO;
import com.jjmsoftsolutions.jblogger.domain.dao.Language;
import com.jjmsoftsolutions.jblogger.rest.Repository;
import com.jjmsoftsolutions.jcommon.rest.ServiceName;
import com.jjmsoftsolutions.jcommon.rest.ServicePathName;

@Named
public class LanguageDAOImpl extends Repository<Language> implements LanguageDAO {

	public Language findByCode(String code) {
		return find(ServiceName.LANGUAGE, ServicePathName.LANGUAGE_FIND_BY_CODE, code);
	}

	public List<Language> findAll() {
		return findAll(ServiceName.LANGUAGE, ServicePathName.LANGUAGE_FIND_ALL);
	}

}
