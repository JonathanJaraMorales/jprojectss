package com.jjmsoftsolutions.jblogger.dao.impl;

import javax.inject.Named;
import com.jjmsoftsolutions.jblogger.dao.UserDAO;
import com.jjmsoftsolutions.jblogger.domain.dao.User;
import com.jjmsoftsolutions.jblogger.rest.Repository;
import com.jjmsoftsolutions.jcommon.exceptions.NotFoundException;
import com.jjmsoftsolutions.jcommon.rest.ServiceName;
import com.jjmsoftsolutions.jcommon.rest.ServicePathName;

@Named
public class UserDAOImpl extends Repository<User> implements UserDAO {

	public boolean insert(User user) {
		try {
			boolean result = insert(ServiceName.USER, ServicePathName.USER_INSERT, user);
			getMessageUtils().addErrorLocalizerMessageToPage("Info.Login.Sign.Up", user.getEmail());
			return result;
		} catch (NotFoundException e) {
			getMessageUtils().addErrorLocalizerMessageToPage("Error.Login.Sign.Up", user.getEmail());
			return false;
		}
	}
	
	public User findUser(String email, String password) {
		try {
			return find(ServiceName.USER, ServicePathName.USER_FIND_BY_EMAIL_PASSWORD, email, password);
		} catch (NotFoundException e) {
			getMessageUtils().addErrorLocalizerMessageToPage("Error.Login.Sign.In", email);
			return null;
		}
	}

}
