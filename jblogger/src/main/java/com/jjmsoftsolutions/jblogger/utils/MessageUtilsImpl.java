package com.jjmsoftsolutions.jblogger.utils;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Named
@SessionScoped
@Singleton
public class MessageUtilsImpl implements MessageUtils {

	@Inject private Localizer localizer;
	
	@Override
	public void addMessageToPage(String summary, String details, Severity severity) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, "", details));
	}

	@Override
	public void addErrorMessageToPage(String details) {
		addMessageToPage("Error", details, FacesMessage.SEVERITY_ERROR);
	}

	@Override
	public void addErrorLocalizerMessageToPage(String details) {
		addMessageToPage("Error", localizer.get(details), FacesMessage.SEVERITY_ERROR);
	}

	@Override
	public void addErrorLocalizerMessageToPage(String details, Object... params) {
		addMessageToPage("Error", localizer.get(details, params), FacesMessage.SEVERITY_ERROR);
	}

	@Override
	public void addSuccessMessageToPage(String details) {
		addMessageToPage("Error", details, FacesMessage.SEVERITY_INFO);
	}

	@Override
	public void addSuccessLocalizerMessageToPage(String details) {
		addMessageToPage("Error", localizer.get(details), FacesMessage.SEVERITY_INFO);
	}

	@Override
	public void addSuccessLocalizerMessageToPage(String details, Object... params) {
		addMessageToPage("Error", localizer.get(details, params), FacesMessage.SEVERITY_INFO);
	}

}
