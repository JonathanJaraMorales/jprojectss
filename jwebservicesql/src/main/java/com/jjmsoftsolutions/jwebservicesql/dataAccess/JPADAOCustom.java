package com.jjmsoftsolutions.jwebservicesql.dataAccess;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.RollbackException;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.validation.ConstraintViolationException;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.eclipse.persistence.exceptions.DatabaseException;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.jjmsoftsolutions.jcommon.exceptions.ConstraintException;
import com.jjmsoftsolutions.jcommon.exceptions.NotFoundException;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Post;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.impl.PostJPAImpl;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class JPADAOCustom<T> implements GenericDAO<T> {

	private EntityManagerFactory emf = null;
	private EntityManager em = null;
	private Class<T> clazz;
	private String PERSISTENCE_UNIT_NAME = "webservices";
	private PersistenceProperties properties = new PersistenceProperties();
	
	@SuppressWarnings("unchecked")
	public JPADAOCustom() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		clazz = (Class<T>) pt.getActualTypeArguments()[0];
	}
	
	public EntityManager getEntityManager() {
		EntityManagerFactory factory = getFactory();
		em = factory.createEntityManager();
		return em;
	}
	
 	public EntityManagerFactory getFactory() {
 		if (emf == null) {
 			Map<String, String> map = new HashMap<String, String>();
 			map.put("javax.persistence.jdbc.password", properties.getPassword());
 			map.put("javax.persistence.jdbc.driver", properties.getDriver());
 			map.put("javax.persistence.jdbc.user", properties.getUser());
 			map.put("javax.persistence.jdbc.url", properties.getUrl());
 			map.put("eclipselink.weaving", "static");
 			map.put("eclipselink.query-results-cache", "true");
 			map.put("eclipselink.connection-pool.initial", "16");
 			map.put("eclipselink.connection-pool.min", "64");
 			map.put("eclipselink.connection-pool.max", "128");
			emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME, map);
 		}
 		return emf;
 	}

	private TypedQuery<T> getQuery(String sql, Map<String, Object> parameters, EntityManager manager) {
		TypedQuery<T> query = manager.createQuery(sql, clazz);
		if (parameters != null) {
			Iterator<Entry<String, Object>> iterator = parameters.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, Object> e = iterator.next();
				if (e.getValue() instanceof Date) {
					query.setParameter(e.getKey(), (Date) e.getValue(), TemporalType.DATE);
				} else {
					query.setParameter(e.getKey(), e.getValue());
				}
			}
		}
		return query;
	}

	@Override
	public void insert(T entity) throws ConstraintViolationException {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			em.persist(entity);
			em.getTransaction().commit();
		} catch (RollbackException ex) {
			final DatabaseException re = (DatabaseException) ex.getCause();
			if(re.getCause() != null && re.getCause() instanceof MySQLIntegrityConstraintViolationException){
				throw new ConstraintException();
	        }
			if (em.getTransaction() != null && em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
   		} finally {
			em.close();
		}
	}
	
	@Override
	public T merge(T t) throws ConstraintViolationException {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			t = em.merge(t);
			em.getTransaction().commit();
			em.getEntityManagerFactory().getCache().evictAll(); 
		} catch (RollbackException ex) {
			final DatabaseException re = (DatabaseException) ex.getCause();
			if(re.getCause() != null && re.getCause() instanceof MySQLIntegrityConstraintViolationException){
				throw new ConstraintException();
	        }
			if (em.getTransaction() != null && em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
     
		} finally {
			em.close();
		}
		return t;
	}

	@Override
	public void deleteById(int id) {
		EntityManager manager = null;
		try {
			manager = getEntityManager();
			manager.getTransaction().begin();
			T object = manager.getReference(getClassImplementation(), id);
			manager.remove(object);
			manager.getTransaction().commit();
			manager.getEntityManagerFactory().getCache().evictAll();
		} finally {
			manager.close();
		}
	}
	
	public void delete(Class<?> clazz, Object key){
		EntityManager manager = null;
		EntityTransaction transaction = null;
		try {
			manager = getEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			T t = getReference(clazz, manager, key);
			manager.remove(t);
			transaction.commit();
		} finally {
			manager.close();
		}
	}

	
	@Override
	public T findById(int id) {
		em = getEntityManager();
		try{
			return em.find(getClassImplementation(), id);
	    }finally{
	    	em.close();
	    }
	}
	
	@SuppressWarnings("unchecked")
	private Class<T> getClassImplementation(){
		Class<T> clazzImpl = null;
		if(clazz.isAnnotationPresent(JsonDeserialize.class)){
			JsonDeserialize annotation = clazz.getAnnotation(JsonDeserialize.class);
			JsonDeserialize info = (JsonDeserialize) annotation;
			clazzImpl = (Class<T>) info.as();
		}
		return clazzImpl;
	}
	
	@SuppressWarnings("unchecked")
	protected T getReference(Class<?> clazz, EntityManager manager, Object key){
		T t = null;
		try {
			t = (T) manager.getReference(clazz, key);
		} catch (EntityNotFoundException e) {
			throw new NotFoundException();
		}
		return t;
	}

	@Override
	public T find(String sql, HashMap<String, Object> parameters) {
		EntityManager manager = getEntityManager();
		try{
			return getQuery(sql, parameters, manager).getSingleResult();
	    } catch (NoResultException e) {
			throw new NotFoundException();
		} finally{
	    	manager.close();
	    }
	}

	@Override
	public List<T> findAll() {
		EntityManager manager = getEntityManager();
		try{
	    	StringBuilder sql = new StringBuilder();
			sql.append("Select U From ").append(getClassImplementation().getSimpleName()).append(" U ");
	    	TypedQuery<T> query = getQuery(sql.toString(), null, manager);
			return query.getResultList();
	    }finally{
	    	manager.close();
	    }
	}

	@Override
	public List<T> findAll(String sql, Map<String, Object> parameters) {
		EntityManager manager = getEntityManager();
		try{
			List<T> list = getQuery(sql.toString(), parameters, manager).getResultList();	
			manager.refresh(list);
			if(list.size() == 0){
				throw new NotFoundException();
			}
			return list;
	    } catch (EntityNotFoundException e) {
			throw new NotFoundException();
		} finally{
	    	manager.close();
	    }
	}

	@Override
	public List<T> findAll(String sql, Map<String, Object> parameters,int begin, int total) {
		EntityManager manager = getEntityManager();
		try{
			TypedQuery<T> query = getQuery(sql, parameters, manager);
			query.setFirstResult(begin);
			query.setMaxResults(total);
			List<T> list = query.getResultList();
			if(list.size() == 0){
				throw new NotFoundException();
			}
			return list;
	    } catch (EntityNotFoundException e) {
			throw new NotFoundException();
		} finally{
	    	manager.close();
	    }
	}
	
	@SuppressWarnings("unused")
	private void testConnection(){
		EntityManager manager = getEntityManager();
		try{
			manager.createNativeQuery("SELECT 1 ").getSingleResult();
	    } finally{
	    	manager.close();
	    }
	}

	@Override
	public T update(T t) {
		return getEntityManager().merge(t);
	}

	@Override
	public int count(String sql, Map<String, Object> params) {
		EntityManager manager = getEntityManager();
		try{
			return ((Long) getQuery(sql, params, manager).getSingleResult()).intValue();
	    } finally{
	    	manager.close();
	    }
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Post> getNativeQuery(String sql) {
		EntityManager manager = null;
		manager = getEntityManager();
        Query query = manager.createNativeQuery(sql, PostJPAImpl.class);
        return query.getResultList();
    }

}
