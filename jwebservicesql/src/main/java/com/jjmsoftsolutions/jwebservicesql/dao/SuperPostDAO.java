package com.jjmsoftsolutions.jwebservicesql.dao;

import com.jjmsoftsolutions.jwebservicesql.dataAccess.GeneralDAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.SuperPost;

public interface SuperPostDAO extends GeneralDAO<SuperPost>{}
