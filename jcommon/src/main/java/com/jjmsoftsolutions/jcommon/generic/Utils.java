package com.jjmsoftsolutions.jcommon.generic;

public class Utils {

	/**
	 * To run this configuration is necessary set the next parameter in eclipse
	 * -DrunInEclipse=true
	 * @return
	 */
	public static boolean isProductionEnvironment(){
		String inEclipseStr = System.getProperty("runInEclipse");
 		return !"true".equals(inEclipseStr);
	}
}
