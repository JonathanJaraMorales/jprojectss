package com.jjmsoftsolutions.jwebservicesql.entity.dao.impl;

import javax.inject.Named;
import javax.persistence.*;
import org.codehaus.jackson.annotate.JsonBackReference;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.PostVideo;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.SuperPost;

@Named
@Entity
@Table(name = "Post_Video")
public class PostVideoJPAImpl implements PostVideo {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_Post_Video")
	private int id;

	@Column(name = "created_Date")
	private String createdDate;

	@ManyToOne(cascade= {CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER, targetEntity = SuperPostJPAImpl.class)
	@JoinColumn(name = "id_Post")
	@JsonBackReference
	private SuperPost superPost;

	@Column(name = "type")
	private String type;

	@Lob
	@Column(name = "url")
	private String url;

	public PostVideoJPAImpl() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public SuperPost getSuperPost() {
		return this.superPost;
	}

	public void setSuperPost(SuperPost superPost) {
		this.superPost = superPost;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}