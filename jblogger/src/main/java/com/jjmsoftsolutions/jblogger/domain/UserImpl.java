package com.jjmsoftsolutions.jblogger.domain;

import java.util.Date;

import javax.inject.Named;

import com.jjmsoftsolutions.jblogger.domain.dao.User;

@Named
public class UserImpl implements User{
	
	private static final long serialVersionUID = 1L;
	private String email;
	private String available;
	private Date createdDate;
	private String nickName;
	private String password;
	private String photo;

	public UserImpl() {
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAvailable() {
		return this.available;
	}

	public void setAvailable(String available) {
		this.available = available;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date created_Date) {
		this.createdDate = created_Date;
	}

	public String getNickName() {
		return this.nickName;
	}

	public void setNickName(String nick_Name) {
		this.nickName = nick_Name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoto() {
		return this.photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
}
