/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.domain;

import java.util.Locale;

import javax.faces.context.FacesContext;

import com.jjmsoftsolutions.jblogger.utils.SessionUtils;

/**
 * Specification of Locale Language
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 * @version 1.0
 * @since 1.0
 */
public class LocaleLanguages {

	private String languageCode;
	private String countryCode;
	private String country;
	
	public LocaleLanguages(String languageCode, String country, String countryCode) {
		setLanguageCode(languageCode);
		setCountry(country);
		setCountryCode(countryCode);
	}

	/**
	 * Return a language code
	 * @return a language code
	 * @see java.lang.String
	 */
	public String getLanguageCode() {
		return languageCode.toUpperCase();
	}

	/**
	 * Set a language code
	 * @return a language code
	 * @param a languge code
	 * @see java.lang.String
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * Retuan a language name
	 * @return a language name
	 * @see java.lang.String
	 */
	public String getLanguage() {
		return SessionUtils.getLanguageName(languageCode);
	}

	/**
	 * Return a locale object, created with the language code
	 * @return a locale object
	 * @see java.util.Locale
	 */
	public Locale getLocale(){
		return new Locale(languageCode);
	}

	/**
	 * Return the country code
	 * @return a country code
	 * @see java.lang.String
	 */
	public String getCountryCode() {
		return countryCode.toUpperCase();
	}

	/**
	 * Set the country code
	 * @param countryCode a country code
	 * @see java.lang.String
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * Return a country nome
	 * @return a country nome
	 * @see java.lang.String
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Set a country nome
	 * @param country
	 * @see java.lang.String
	 */
	public void setCountry(String country) {
		this.country = country;
	}

}
