package com.jjmsoftsolutions.jblogger.domain;

import javax.inject.Named;
import javax.servlet.http.Part;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.jjmsoftsolutions.jblogger.domain.dao.Post;
import com.jjmsoftsolutions.jblogger.domain.dao.PostAttachment;

@Named
public class PostAttachmentImpl implements PostAttachment {

	private static final long serialVersionUID = 1L;
	private int id;
	private String description;
	private String name;
	private int size;
	private String type;
	private String url;
	private Post post;
	private Part part;

	public PostAttachmentImpl() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSize() {
		return this.size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	@JsonIgnore
	public void setPart(Part part) {
		this.part = part;
	}

	@JsonIgnore
	public Part getPart() {
		return part;
	}
}
