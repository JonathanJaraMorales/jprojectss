/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.jjmsoftsolutions.jblogger.converter;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

import com.jjmsoftsolutions.jblogger.domain.Tag_Translation;
import com.jjmsoftsolutions.jblogger.utils.StringUtils;
/**
 * Allows to convert a tag 
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 * @version 1.0
 * @since 1.0
 * @see javax.faces.convert.Converter
 */
@FacesConverter("tagConverter")
@Named
public class TagConverter implements Converter {
	
	private List<Tag_Translation> temporalTags;

	/**
	 * Return a tag object
	 * @see com.jjmsoftsolutions.jblogger.domain.Tag_Translation
	 * @throws javax.faces.convert.ConverterException
	 */
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		for(Tag_Translation tag : temporalTags){
			if(tag.getId_Tag_Translation()== StringUtils.getInt(value)){
				return tag;
			}
		}
		return null;
	}

	/**
	 * Return the id of the tag translation
	 */
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if(value != null) {
            return String.valueOf(((Tag_Translation) value).getId_Tag_Translation());
        }
        return null;
	}
	
	public List<Tag_Translation> getTemporalTags() {
		return temporalTags;
	}

	public void setTemporalTags(List<Tag_Translation> temporalTags) {
		this.temporalTags = temporalTags;
	}
	
	

}
