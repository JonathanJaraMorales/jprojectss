package com.jjmsoftsolutions.jwebservicesql.entity.dao.impl;

import javax.inject.Named;
import javax.persistence.*;

import com.jjmsoftsolutions.jwebservicesql.entity.dao.Parameter;

@Named
@Entity
@Table(name = "Parameter")
public class ParameterJPAImpl implements Parameter {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="code")
	private String code;

	@Lob
	@Column(name="content")
	private String content;

	public ParameterJPAImpl() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}