package com.jjmsoftsolutions.jwebservicesql.dao;

import java.util.List;

import com.jjmsoftsolutions.jwebservicesql.dataAccess.GeneralDAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Post;

public interface PostDAO extends GeneralDAO<Post>{
	public List<Post> findByLanguage(String language, int begin, int end);
	public int countByLanguage(String language);
	public List<Post> findAllPostByTag(String tag, int begin, int end);
	public int countByTag(String tag);
	public void insert(Post post);
	public List<Post> searchEngine(String searchEngine, int begin, int end);
	public Post findLanguageSuperPost(String language, int id);
	public int countBySearchEngine(String searchEngine);
}
