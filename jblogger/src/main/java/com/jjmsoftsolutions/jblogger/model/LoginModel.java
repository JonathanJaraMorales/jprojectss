package com.jjmsoftsolutions.jblogger.model;

import javax.inject.Inject;
import javax.inject.Named;
import com.jjmsoftsolutions.jblogger.dao.UserDAO;
import com.jjmsoftsolutions.jblogger.domain.dao.User;

@Named
public class LoginModel {

	@Inject private UserDAO dao;
	
	public User login(String email, String password) {
		return dao.findUser(email, password);
	}

	public boolean insert(User user) {
		return dao.insert(user);
	}

}
