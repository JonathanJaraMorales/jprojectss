package com.jjmsoftsolutions.jwebservicesql.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import com.jjmsoftsolutions.jcommon.exceptions.EntityValidatorException;
import com.jjmsoftsolutions.jcommon.exceptions.NotFoundException;
import com.jjmsoftsolutions.jcommon.generic.Error;
import com.jjmsoftsolutions.jcommon.generic.ResponseStatus;
import com.jjmsoftsolutions.jcommon.rest.ServiceName;
import com.jjmsoftsolutions.jwebservicesql.dao.PostDAO;
import com.jjmsoftsolutions.jwebservicesql.dao.SuperPostDAO;
import com.jjmsoftsolutions.jwebservicesql.dao.TagDAO;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Language;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Post;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.PostVideo;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.SuperPost;
import com.jjmsoftsolutions.jwebservicesql.entity.dao.Tag;
import com.jjmsoftsolutions.jwebservicesql.entity.parser.dao.PostParser;
import com.jjmsoftsolutions.jwebservicesql.service.LanguageService;
import com.jjmsoftsolutions.jwebservicesql.service.ParameterService;
import com.jjmsoftsolutions.jwebservicesql.service.PostService;
import com.jjmsoftsolutions.jwebservicesql.wrapper.EntityValidator;

@Component
@Path(ServiceName.POST)
public class PostServiceImpl implements PostService {

	@Inject private PostDAO dao;
	@Inject private TagDAO tagDao;
	@Inject private PostParser parser;
	@Inject private ParameterService parameterService;
	@Inject private SuperPostDAO superPostDao;
	@Inject private LanguageService languageService;
	
	@Override
	public Response findById(int id) {
		try {
			Post post = autoIncrementView(dao.findById(id));
			removeSuperPostFromVideo(post);
			return Response.ok().entity(post).type(MediaType.APPLICATION_JSON).build();
		} catch (NotFoundException e) {
			return Response.status(Error.POST_FIND_BY_ID.getCode()).entity(Error.POST_FIND_BY_ID.getMessage()).type(MediaType.APPLICATION_JSON).build();
		} 
	}
	
	@Override
	public Response findByLanguage(String language, int begin, int end) {
		try {
			List<Post> postList = dao.findByLanguage(language, begin, end);
			return Response.ok().entity(postList).type(MediaType.APPLICATION_JSON).build();
		} catch (NotFoundException e) {
			return Response.status(Error.POST_FIND_BY_LANGUAGE.getCode()).entity(Error.POST_FIND_BY_LANGUAGE.getMessage()).type(MediaType.APPLICATION_JSON).build();
		} 
	}
	
	@Override
	public Response insert(String jsonRequest){
		try {
			Post post = parser.jsonToPost(jsonRequest);
			if (validateEntity(post)) {
				Response response = languageService.findByLanguageCode(post.getLanguage().getCode());
				if (response.getStatus() == ResponseStatus.OK.getStatus()) {
					post.setLanguage(cast(response.getEntity(), Language.class));
				} else {
					return response;
				}
				if (post.getSuperPost() != null && post.getSuperPost().getId() > 0) {
					int superPostId = post.getSuperPost().getId();
					SuperPost superPost = superPostDao.findById(superPostId);
					superPost.addPost(post);
					superPostDao.merge(superPost);
				} else {
					dao.insert(post);
				}
			}
			return Response.ok().type(MediaType.APPLICATION_JSON).build();
		} catch (NotFoundException e) {
			return Response.status(Error.POST_INSERT.getCode()).entity(Error.POST_INSERT.getMessage()).type(MediaType.APPLICATION_JSON).build();
		} catch (EntityValidatorException e){
			return Response.status(e.getCode()).entity(e.getMessage()).type(MediaType.APPLICATION_JSON).build();
		}
	}
	
	@Override
	public Response findByTag(String tag, int begin, final int end){
		try {
			List<Post> list = dao.findAllPostByTag(tag, begin, end);
			return Response.ok().entity(list).type(MediaType.APPLICATION_JSON).build();
		} catch (NotFoundException e) {
			return Response.status(Error.POST_FIND_BY_TAG.getCode()).entity(Error.POST_FIND_BY_TAG.getMessage()).type(MediaType.APPLICATION_JSON).build();
		} 
	}
	
	@Override
	public Response countByTag(String tag) {
		try {
			int count = dao.countByTag(tag);
			return Response.ok().entity(count).build();	  
		} catch (NotFoundException e) {
			return Response.status(Error.POST_COUNT_BY_TAG.getCode()).entity(Error.POST_COUNT_BY_TAG.getMessage()).type(MediaType.APPLICATION_JSON).build();
		} 
	}
	
	@Override
	public Response countByLanguage(String language) {
		try {
			Integer count = dao.countByLanguage(language);
			return Response.ok().entity(count).build();
		} catch (NotFoundException e) {
			return Response.status(Error.POST_COUNT_BY_LANGUAGE.getCode()).entity(Error.POST_COUNT_BY_LANGUAGE.getMessage()).type(MediaType.APPLICATION_JSON).build();
		} 
	}

	@Override
	public Response findByTagId(int id, int begin, int end) {
		try {
			Tag tag = tagDao.findById(id);
			List<Post> list = dao.findAllPostByTag(tag.getName(), begin, end);
			return Response.ok().entity(list).type(MediaType.APPLICATION_JSON).build();
		} catch (NotFoundException e) {
			return Response.status(Error.POST_FIND_BY_TAG_ID.getCode()).entity(Error.POST_FIND_BY_TAG_ID.getMessage()).type(MediaType.APPLICATION_JSON).build();
		} 
	}

	@Override
	public Response deleteByCode(int id) {
		try {
			dao.deleteById(id);
			return Response.ok().entity(ResponseStatus.OK.getMessage()).type(MediaType.APPLICATION_JSON).build();
		} catch (NotFoundException e) {
			return Response.status(Error.POST_DELETE.getCode()).entity(Error.POST_DELETE.getMessage()).type(MediaType.APPLICATION_JSON).build();
		}
	}

	@Override
	public Response searchEngine(String searchEngine, int begin, int end) throws UnsupportedEncodingException {
		try {
			byte ptext[] = searchEngine.getBytes("ISO-8859-1"); 
			List<Post> list = dao.searchEngine(new String(ptext, "UTF-8"), begin, end);
			return Response.ok().entity(list).type(MediaType.APPLICATION_JSON).build();
		} catch (NotFoundException e) {
			return Response.status(Error.POST_SEARCH_ENGINE.getCode()).entity(Error.POST_SEARCH_ENGINE.getMessage()).type(MediaType.APPLICATION_JSON).build();
		} 
	}

	@Override
	public Response findByLanguageSuperPost(String language, int id) {
		try {
			Post post = dao.findLanguageSuperPost(language, id);
			return Response.ok().entity(post).type(MediaType.APPLICATION_JSON).build();
		} catch (NotFoundException e) {
			return Response.status(Error.POST_FIND_BY_LANGUAGE_SUPER_POST.getCode()).entity(Error.POST_FIND_BY_LANGUAGE_SUPER_POST.getMessage()).type(MediaType.APPLICATION_JSON).build();
		} 
	}

	@Override
	public Response countBySearchEngine(String searchEngine) throws UnsupportedEncodingException{
		try {
			byte ptext[] = searchEngine.getBytes("ISO-8859-1"); 
			int count = dao.countBySearchEngine(new String(ptext, "UTF-8"));
			return Response.ok().entity(count).type(MediaType.APPLICATION_JSON).build();
		} catch (NotFoundException e) {
			return Response.status(Error.POST_COUNT_BY_SEARCH_ENGINE.getCode()).entity(Error.POST_COUNT_BY_SEARCH_ENGINE.getMessage()).type(MediaType.APPLICATION_JSON).build();
		} 
	}
	
	private Post autoIncrementView(Post post){
		if(parameterService.findByCode("POST_INFO_AUTO_INCREMENT_COUNT_VIEWS").getContent().equals("1")){
			int view = post.getViews() + 1;
			post.setViews(view);
			return dao.merge(post);
		}
		return post;
	}
	
	private void removeSuperPostFromVideo(Post post){
		if(post != null && post.getSuperPost() != null){
			List<PostVideo> videos = post.getSuperPost().getVideos();
			if (videos != null) {
				for (PostVideo video : videos) {
					video.setSuperPost(null);
				}
			}
			
		}
	}
	
	private boolean validateEntity(Post post){
		if (post.getLanguage() == null) {
			throw new EntityValidatorException(Error.POST_INSERT_VALIDATION_LANGUAGE.getCode(), Error.POST_INSERT_VALIDATION_LANGUAGE.getMessage());
		}
		return EntityValidator.processAnnotations(post);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T cast(Object object, Class<T> clazz){		
		return (T) object;
	}
}
