/*
 * Copyright (c) 2014, JJMSoftSolutions. All rights reserved.
 * JJMSOFTSOLUTIONS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.jjmsoftsolutions.jblogger.dao.impl;

import java.util.List;
import javax.inject.Named;
import com.jjmsoftsolutions.jblogger.dao.TagDAO;
import com.jjmsoftsolutions.jblogger.domain.Tag_Translation;
import com.jjmsoftsolutions.jblogger.rest.Repository;

/**
 * Allow to create the implementation of {@code Tag_Translation}
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 * @version 1.0
 * @since 1.0
 * @see com.jjmsoftsolutions.jblogger.domain.Tag_Translation
 * @see com.jjmsoftsolutions.jblogger.dao.TagDAO
 * @see com.jjmsoftsolutions.jblogger.rest.Repository
 */
@Named
public class TagDAOImpl extends Repository<Tag_Translation> implements TagDAO{

	public List<Tag_Translation> findTagMenuByLanguage(String language) {
		return findAll("tag", "findPrincipalTagsByLanguage", language);
	}

	public List<Tag_Translation> findTagsByLanguageAndTitle(String language, String title) {
		return findAll("tag", "findTagsByLanguageAndTitle", language, title);
	}

	public Tag_Translation findTagById(int id) {
		return find("tag", "findTagById", id);
	}

}
